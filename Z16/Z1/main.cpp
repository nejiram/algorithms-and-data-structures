#include <iostream>
#include <stdexcept>

template<typename TipKljuca, typename TipVrijednosti>
class Mapa{
    public:
    Mapa(){};
    virtual ~Mapa(){};
    virtual TipVrijednosti operator[](const TipKljuca &k)const=0;
    virtual TipVrijednosti &operator[](const TipKljuca &k)=0;
    virtual int brojElemenata() const=0;
    virtual void obrisi()=0;
    virtual void obrisi(const TipKljuca &k)=0;
};

template<typename TipKljuca, typename TipVrijednosti>
class NizMapa: public Mapa<TipKljuca, TipVrijednosti>{
    private:
    std::pair<TipKljuca, TipVrijednosti> *niz;
    int br_elemenata, kapacitet;
    public:
    ~NizMapa(){
        delete[] niz;
        niz=nullptr;
    }
    
    NizMapa():br_elemenata(0), kapacitet(10) {
        niz=new std::pair<TipKljuca, TipVrijednosti> [10];
    }
    
    NizMapa(const NizMapa &m){
        br_elemenata=m.br_elemenata;
        kapacitet=m.kapacitet;
        niz=new std::pair<TipKljuca, TipVrijednosti> [kapacitet];
        
        for(int i=0; i<br_elemenata; i++)
            niz[i]=m.niz[i];
    }
    
    NizMapa &operator=(const NizMapa &m){
        if(this==&m) return *this;
        
        if(br_elemenata!=0){
        delete[] niz;
        }
        br_elemenata=m.br_elemenata;
        kapacitet=m.kapacitet;
        niz=new std::pair<TipKljuca, TipVrijednosti> [kapacitet];
        
        for(int i=0; i<br_elemenata; i++)
            niz[i]=m.niz[i];
            
        return *this;
    }
    
    TipVrijednosti operator [](const TipKljuca &k) const{
        for(int i=0;i<br_elemenata;i++){
            if(niz[i].first==k) return niz[i].second;
        }
        return TipVrijednosti();
    }
    
    TipVrijednosti &operator[](const TipKljuca &k){
        
            for(int i=0; i<br_elemenata; i++){
                if(niz[i].first==k) return niz[i].second;
            }
            if(kapacitet>br_elemenata){
                niz[br_elemenata++].first=k;
                niz[br_elemenata-1].second=TipVrijednosti();
            }
            else{
                kapacitet*=2;
                std::pair<TipKljuca, TipVrijednosti> *tmp;
                tmp=niz;
                //delete[] niz;
                niz=new std::pair<TipKljuca, TipVrijednosti>[kapacitet];
                for(int i=0; i<br_elemenata; i++){
                    niz[i]=tmp[i];
                }
                delete[] tmp;
                tmp=nullptr;
                niz[br_elemenata++].first=k;
                niz[br_elemenata-1].second=TipVrijednosti();
            }
            return niz[br_elemenata-1].second;
    }
    
    int brojElemenata() const{
        return br_elemenata;
    }
    
    void obrisi(){
        delete[] niz;
        niz=new std::pair<TipKljuca, TipVrijednosti>[kapacitet];
        br_elemenata=0;
    }
    
    void obrisi(const TipKljuca &k){
        bool ima(false);
        for(int i=0; i<br_elemenata; i++){
            if(niz[i].first==k) ima=true;
        }
        if(!ima) throw std::domain_error("Ne postoji par sa unesenim kljucem");
        for(int i=0; i<br_elemenata; i++){
            if(niz[i].first==k){
                for(int j=i;j<br_elemenata-1; j++){
                    niz[j]=niz[j+1];
                }
                br_elemenata--;
                break;
            }
        }
    }
};

void Test1(){
    NizMapa<int, std::string> m;
    m[1]="ASP";
    m[2]="DM";
    m[3]="NA";
    m[4]="LD";
    m[5]="OBP";
    m[6]="RPR";
    for(int i=0;i<m.brojElemenata();i++)
        std::cout<<m[i]<<" ";
    std::cout<<std::endl;
}

void Test2(){
    NizMapa<int, std::string> m;
    m[1]="ASP";
    m[2]="DM";
    m[3]="NA";
    m[4]="LD";
    m[5]="OBP";
    m[6]="RPR";
    std::cout<<m[1]<<std::endl;
}

void Test3(){
    NizMapa<int, std::string> m;
    m[1]="ASP";
    m[2]="DM";
    m[3]="NA";
    m[4]="LD";
    m[5]="OBP";
    m[6]="RPR";
    m.obrisi(5);
    m.obrisi(6);
    for(int i=0;i<m.brojElemenata();i++)
        std::cout<<m[i]<<" ";
    std::cout<<std::endl;
}

void Test4(){
    NizMapa<int, std::string> m;
    m[1]="ASP";
    m[2]="DM";
    m[3]="NA";
    m[4]="LD";
    m[5]="OBP";
    m[6]="RPR";
    m.obrisi(2);
    m.obrisi(4);
    m.obrisi(6);
    for(int i=0;i<m.brojElemenata();i++)
        std::cout<<m[i]<<" ";
    std::cout<<std::endl;
}

void Test5(){
    try{
        NizMapa<int, std::string> m;
        m[1]="ASP";
        m[2]="DM";
        m[3]="NA";
        m[4]="LD";
        m[5]="OBP";
        m[6]="RPR";
        m.obrisi(7);
    }catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
}

void Test6(){
    NizMapa<int, std::string> m;
        m[1]="ASP";
        m[2]="DM";
        m[3]="NA";
        m[4]="LD";
        m[5]="OBP";
        m[6]="RPR";
    std::cout<<"Broj elemenata prije brisanja: "<<m.brojElemenata()<<" ";
    m.obrisi();
    std::cout<<"Broj elemenata nakon brisanja: "<<m.brojElemenata()<<std::endl;
}

void Test7(){
    NizMapa<int, std::string> m;
        m[1]="ASP";
        m[2]="DM";
        m[3]="NA";
        m[4]="LD";
        m[5]="OBP";
        m[6]="RPR";
    NizMapa<int, std::string> m2(m);
    std::cout<<m2.brojElemenata()<<std::endl;
}

void Test8(){
    NizMapa<int, std::string> m;
        m[1]="ASP";
        m[2]="DM";
        m[3]="NA";
        m[4]="LD";
        m[5]="OBP";
        m[6]="RPR";
    NizMapa<int, std::string> m2;
    m2[1]="IM1";
    m2[2]="IF1";
    m2[3]="LAG";
    m2[4]="OR";
    m2[5]="OE";
    m2=m;
    std::cout<<m2.brojElemenata()<<std::endl;
}

void Test9(){
    NizMapa<int, std::string> m;
        m[1]="ASP";
        m[2]="DM";
        m[3]="NA";
        m[4]="LD";
        m[5]="OBP";
        m[6]="RPR";
    NizMapa<int, std::string> m2;
    m2[1]="IM1";
    m2[2]="IF1";
    m2[3]="LAG";
    m2[4]="OR";
    m2[5]="OE";
    m=m2;
    std::cout<<m.brojElemenata()<<std::endl;
}

void Test10(){
    NizMapa<int, std::string> m;
        m[1]="ASP";
        m[2]="DM";
        m[3]="NA";
        m[4]="LD";
        m[5]="OBP";
        m[6]="RPR";
    NizMapa<int, std::string> m2;
    m2[1]="IM1";
    m2[2]="IF1";
    m2[3]="LAG";
    m2[4]="OR";
    m2[5]="OE";
    m2=m2;
    std::cout<<m2.brojElemenata()<<std::endl;
}



int main() {
    Test1();
    Test2();
    Test3();
    Test4();
    Test5();
    Test6();
    Test7();
    Test8();
    Test9();
    Test10();
    return 0;
}
