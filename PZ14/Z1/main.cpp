#include <iostream>
#include <stdexcept>

template<typename Tip>
class Red{
    private:
    int br_elemenata;
    struct Cvor{
        Tip info;
        Cvor *veza;
    };
    Cvor *prvi, *tren;
    public:
    
    Red():br_elemenata(0),prvi(nullptr),tren(nullptr){}
    
    ~Red(){
        if(br_elemenata!=0)
        while(prvi!=nullptr){
            Cvor *p=prvi->veza;
            delete prvi;
            prvi=p;
        }
    }
    
    Red(const Red &r){
        br_elemenata=r.br_elemenata;
        if(br_elemenata!=0){
            prvi=new Cvor;
            prvi->info=r.prvi->info;
            prvi->veza=nullptr;
            Cvor *tmp(r.prvi->veza);
            tren=prvi;
            if(tmp!=nullptr)
            do{
                Cvor *pom=new Cvor;
                pom->info=tmp->info;
                pom->veza=nullptr;
                tren->veza=pom;
                tren=pom;
                pom=nullptr;
                tmp=tmp->veza;
            }while(tmp!=nullptr);
            else {
                prvi=nullptr;
                tren=nullptr;
            }
        }
    }
    
    void brisi(){
        while(prvi!=nullptr){
            Cvor *p=prvi->veza;
            delete prvi;
            prvi=p;
        }
        br_elemenata=0;
    }
    
    void stavi(const Tip &el){
        if(br_elemenata==0){
            prvi=new Cvor;
            prvi->info=el;
            prvi->veza=nullptr;
            br_elemenata++;
            tren=prvi;
        }
        else if(br_elemenata>0){
            Cvor *p=new Cvor;
            p->veza=nullptr;
            p->info=el;
            tren->veza=p;
            tren=p;
            p=nullptr;
            br_elemenata++;
        }
    }
    
    Tip skini(){
        if(br_elemenata==0) throw std::domain_error("Red je prazan !");
        Tip tmp=prvi->info;
        auto pom(prvi->veza);
        delete prvi;
        prvi=pom;
        pom=nullptr;
        br_elemenata--;
        return tmp;
    }
    
    Tip &celo(){
        if(br_elemenata==0) throw std::domain_error("Red je prazan !");
        return prvi->info;
    }
    
    int brojElemenata(){
        return br_elemenata;
    }
    
    Red &operator=(const Red &r){
        if(this==&r) return *this;
        while(prvi!=nullptr){
            Cvor *p=prvi->veza;
            delete prvi;
            prvi=p;
        }
        br_elemenata=r.br_elemenata;
        if(br_elemenata!=0){
            prvi=new Cvor;
            prvi->info=r.prvi->info;
            prvi->veza=nullptr;
            Cvor *tmp(r.prvi->veza);
            tren=prvi;
            if(tmp!=nullptr)
            do{
                Cvor *pom=new Cvor;
                pom->info=tmp->info;
                pom->veza=nullptr;
                tren->veza=pom;
                tren=pom;
                pom=nullptr;
                tmp=tmp->veza;
            }while(tmp!=nullptr);
        }
        else {
            prvi=nullptr;
            tren=nullptr;
        }
        return *this;
    }
};

void Test_Konstr(){
    Red<char> r;
    std::cout<<r.brojElemenata()<<std::endl;
}

void Test_Konstr_Kopije1(){
    Red<int> r1;
    for(int i=0;i<1000;i++)
    r1.stavi(i);
    Red<int> r2(r1);
    std::cout<<r2.brojElemenata()<<std::endl;
}

void Test_Konstr_Kopije2(){
    Red<int> r1;
    Red<int> r2(r1);
    std::cout<<r2.brojElemenata()<<std::endl;
}

void Test_Brisi(){
    Red<int> r;
    for(int i=0;i<10000;i++)
    r.stavi(i);
    r.brisi();
    std::cout<<r.brojElemenata()<<std::endl;
}

void Test_Stavi_Jedan(){
    Red<std::string> r;
    r.stavi("Jedini element reda");
    std::cout<<r.celo()<<std::endl;
}

void Test_Stavi(){
    Red<double> r;
    for(double i=0.2;i<=1000;i+=0.2)
    r.stavi(i);
    std::cout<<r.brojElemenata()<<std::endl;
}

void Test_Celo1(){
    try{
    Red<char> r;
    std::cout<<r.celo()<<std::endl;
    } catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
}

void Test_Celo2(){
    Red<int> r;
    for(int i=1;i<=10;i++)
    r.stavi(i);
    std::cout<<r.celo()<<std::endl;
}

void Test_Skini_Jedan(){
    Red<int> r;
    for(int i=1;i<=10;i++)
    r.stavi(i);
    r.skini();
    std::cout<<r.celo()<<std::endl;
}

void Test_Skini_Sve(){
    try{
    Red<double> r;
    for(double i=0.2;i<=1000;i+=0.2)
    r.stavi(i);
    for(int i=0;i<5000;i++)
    r.skini();
    std::cout<<r.brojElemenata()<<std::endl;
    } catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
}

void Test_Skini_Prazan(){
    try{
        Red<int> r;
        r.skini();
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
}

void Test_Dodjela1(){
    Red<int> r;
    for(int i=1;i<=100;i++)
    r.stavi(i);
    r=r;
    std::cout<<r.brojElemenata()<<std::endl;
}

void Test_Dodjela2(){
    Red<int> r1;
    for(int i=1;i<=1000000;i++)
    r1.stavi(i);
    Red<int> r2;
    r2=r1;
    std::cout<<r2.brojElemenata()<<std::endl;
}

int main() {
    Test_Konstr();
    Test_Konstr_Kopije1();
    Test_Konstr_Kopije2();
    Test_Brisi();
    Test_Stavi_Jedan();
    Test_Stavi();
    Test_Celo1();
    Test_Celo2();
    Test_Skini_Jedan();
    Test_Skini_Sve();
    Test_Skini_Prazan();
    Test_Dodjela1();
    Test_Dodjela2();
    return 0;
}
