#include <iostream>

int nzd(int x, int y){
    if(x==y || y==0) return x;
    return nzd(y, x%y);
}

int fib2_0(int n, int x=1, int y=0){
    if(n==1) return x;
    if(n==0) return y;
    return fib2_0(n-2,x,y)+fib2_0(n-1,x,y);
}

int main() {
    //std::cout << "Pripremna Zadaca Za Vjezbu 5, Zadatak 1";
    int m=1250,n=150;
    std::cout<<nzd(m,n)<<std::endl;
    return 0;
}
