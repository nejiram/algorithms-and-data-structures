#include <iostream>
#include <vector>
#include <algorithm>
#include <stdexcept>
using namespace std;

void pomRadixSort(vector<int> &a, vector<vector<int>> &tmp, int step){
    for(int i=0;i<int(a.size());i++){
        tmp[(a[i]%step)/(step/10)].push_back(a[i]);
    }
    a.clear();
    for(int i=0; i<int(tmp.size());i++){
        for(int j=0; j<int(tmp[i].size()); j++){
            a.push_back(tmp[i][j]);
        }
    }
    if(int(tmp[0].size())!=int(a.size())){
        for(int i=0; i<int(tmp.size());i++){
            tmp[i].clear();
        }
        pomRadixSort(a,tmp,step*10);
    }
}

void radixSort(vector<int> &a){
    vector<vector<int>> tmp(10);
    pomRadixSort(a, tmp, 10);
}

bool jeLiList(vector<int> &a, int i, int n){
    return (i>=n/2 && i<n);
}

int roditelj(int i){
    return int((i-1)/2);
}

int lijevoDijete(int i){
    return 2*i+1;
}

int desnoDijete(int i){
    return 2*i+2;
}

void popraviDolje(vector<int> &a, int i, int n){
    int veci, dd;
    while(!(jeLiList(a, i, n))){
        veci=lijevoDijete(i);
        dd=desnoDijete(i);
        if(dd<n && a[dd]>a[veci]) veci=dd;
        if(a[i]>a[veci]) return;
        int pom(a[i]);
        a[i]=a[veci];
        a[veci]=pom;
        i=veci;
    }
}

void stvoriGomilu(vector<int> &a){
    for(int i=int(a.size())/2; i>=0; i--)
        popraviDolje(a, i, int(a.size()));
}

void popraviGore(vector<int> &a, int i){
    while(i!=0 && a[i]>a[roditelj(i)]){
        int pom(a[i]);
        a[i]=a[roditelj(i)];
        a[roditelj(i)]=pom;
        i=roditelj(i);
    }
}

void umetniUGomilu(vector<int> &a, int umetnuti, int &velicina){
    if(int(a.size())<=velicina){
        a.push_back(umetnuti);
        velicina=int(a.size());
    }
    else{
        a[velicina]=umetnuti;
        velicina++;
    }
    popraviGore(a, velicina-1);
} 

int izbaciPrvi(vector<int> &a, int &velicina){
    if(velicina==0) throw domain_error("Gomila je prazna");
    velicina--;
    int pom(a[0]);
    a[0]=a[velicina];
    a[velicina]=pom;
    if(velicina!=0) popraviDolje(a,0,velicina);
    return a[velicina];
}

void gomilaSort(vector<int> &a){
    stvoriGomilu(a);
    int n(int(a.size()));
    for(int i=0; i<int(a.size())-1; i++)
        izbaciPrvi(a,n);
}

int main() {
    std::cout << "Zadaća 3, Zadatak 1"<<endl;
    
    try{
        vector<int> v;
        v.push_back(1);
        v.push_back(7);
        v.push_back(28);
        v.push_back(372);
        v.push_back(373);
        v.push_back(500);
        v.push_back(1234);
        v.push_back(5432);
        v.push_back(123456);
        v.push_back(100000000);
        cout<<"Pocetni vektor: ";
        for(int i=0; i<int(v.size()); i++)
            cout<<v[i]<<" ";
        cout<<endl;
        
        stvoriGomilu(v);
        cout<<"Nakon poziva stvoriGomilu: ";
        for(int i=0; i<int(v.size()); i++)
            cout<<v[i]<<" ";
        cout<<endl;
        
        int velicina(int(v.size()));
        umetniUGomilu(v, 0, velicina);
        umetniUGomilu(v, 99999999, velicina);
        umetniUGomilu(v, 10000, velicina);
        cout<<"Nakon umetanja 3 nova elementa: ";
        for(int i=0; i<int(v.size()); i++)
            cout<<v[i]<<" ";
        cout<<endl;
        
        izbaciPrvi(v, velicina);
        izbaciPrvi(v, velicina);
        izbaciPrvi(v, velicina);
        cout<<"Nakon 3 poziva izbaciPrvi: ";
        for(int i=0; i<int(v.size()); i++)
            cout<<v[i]<<" ";
        cout<<endl;
        
        gomilaSort(v);
        cout<<"Nakon poziva gomilaSort: ";
        for(int i=0; i<int(v.size()); i++)
            cout<<v[i]<<" ";
        cout<<endl;
        
        
        for(int i=0; i<int(v.size())+1;i++)
            izbaciPrvi(v, velicina);
        
    }catch(domain_error izuzetak){
        cout<<izuzetak.what();
    }
       
    return 0;
}
