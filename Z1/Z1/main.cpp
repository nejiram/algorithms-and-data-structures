#include <iostream>
#include <stdexcept>

template<typename Tip>
class Iterator;

template<typename Tip>
class Lista{
    private:
    void operator=(const Lista &l);
    Lista(const Lista &l);
    public:
  Lista(){};
  virtual ~Lista(){};
  //Lista(const Lista &l);
  virtual int brojElemenata()const=0;
  //virtual Tip &trenutni()=0;
  virtual Tip &trenutni() const=0;
  virtual bool prethodni()=0;
  virtual bool sljedeci()=0;
  virtual void pocetak()=0;
  virtual void kraj()=0;
  virtual void obrisi()=0;
  virtual void dodajIspred(const Tip &el)=0;
  virtual void dodajIza(const Tip &el)=0;
  //virtual Tip &operator[](int i)=0;
  virtual Tip &operator[](int i)const=0;
};

template<typename Tip>
class DvostrukaLista:public Lista<Tip>{
    private:
    int br_elemenata, pozicija;
    struct Cvor{
        Tip info;
        Cvor *naprijed, *nazad;
    };
    Cvor *start, *end, *tren;
    public:
    DvostrukaLista():br_elemenata(0),pozicija(0),start(nullptr),end(nullptr),tren(nullptr){}
    
    DvostrukaLista(const DvostrukaLista &lista){
        br_elemenata=lista.br_elemenata;
        pozicija=lista.pozicija;
        start=nullptr;
        end=nullptr;
        tren=nullptr;
        if(br_elemenata>0){
            start=new Cvor;
            start->info=lista.start->info;
            start->nazad=nullptr;
            end=start;
            auto tmp(lista.start->naprijed);
            while(tmp->naprijed!=nullptr){
                auto tmp2=new Cvor;
                tmp2->info=tmp->info;
                tmp2->nazad=end;
                end->naprijed=tmp2;
                tmp2->naprijed=nullptr;
                end=tmp2;
                tmp2=nullptr;
                if(tmp==lista.tren) tren=tmp;
                tmp=tmp->naprijed;
            }
        }
    }
    
    ~DvostrukaLista(){
        while(start!=nullptr){
            auto p(start->naprijed);
            delete start;
            start=p;
        }
    }
    
    int brojElemenata() const{
        return br_elemenata;
    }
    
    Tip &trenutni() const{
        if(br_elemenata==0) throw std::domain_error("Lista je prazna!");
        return tren->info;
    }
    
    bool prethodni(){
        if(br_elemenata==0) throw std::domain_error("Lista je prazna!");
        if(tren==start) return false;
        tren=tren->nazad;
        pozicija--;
        return true;
    }
    
    bool sljedeci(){
        if(br_elemenata==0) throw std::domain_error("Lista je prazna!");
        if(tren==end) return false;
        tren=tren->naprijed;
        pozicija++;
        return true;
    }
    
    void pocetak(){
        if(br_elemenata==0) throw std::domain_error("Lista je prazna!");
        tren=start;
        pozicija=0;
    }
    
    void kraj(){
        if(br_elemenata==0) throw std::domain_error("Lista je prazna!");
        tren=end;
        pozicija=br_elemenata-1;
    }
    
    void obrisi(){
        if(br_elemenata==0) throw std::domain_error("Lista je prazna!");
        if(br_elemenata==1){
            delete tren;
            tren=nullptr;
            start=nullptr;
            end=nullptr;
            br_elemenata--;
            pozicija=0;
        }
        else if(tren==start){
            auto tmp(start->naprijed);
            delete tren;
            tren=tmp;
            tmp=nullptr;
            tren->nazad=nullptr;
            start=tren;
            br_elemenata--;
            pozicija=0;
        }
        else if(tren==end){
            auto tmp(end->nazad);
            delete tren;
            tren=tmp;
            tmp=nullptr;
            tren->naprijed=nullptr;
            end=tren;
            br_elemenata--;
            pozicija=br_elemenata-1;
        }
        else{
            auto forw(tren->naprijed), back(tren->nazad);
            delete tren;
            forw->nazad=back;
            back->naprijed=forw;
            tren=forw;
            forw=nullptr; back=nullptr;
            br_elemenata--;
            pozicija++;
        }
    }
    
    void dodajIspred(const Tip &el){
        if(br_elemenata==0){
            start=new Cvor;
            start->info=el;
            start->naprijed=nullptr;
            start->nazad=nullptr;
            tren=start;
            end=tren;
            br_elemenata++;
            pozicija=0;
        }
        else if(tren==start){
            auto tmp=new Cvor;
            tmp->info=el;
            tmp->nazad=nullptr;
            tmp->naprijed=tren;
            tren->nazad=tmp;
            start=tmp;
            tmp=nullptr;
            br_elemenata++;
            pozicija++;
        }
        else{
            auto tmp=new Cvor;
            auto pom(tren->nazad);
            tmp->info=el;
            tmp->naprijed=tren;
            tmp->nazad=pom;
            pom->naprijed=tmp;
            tren->nazad=tmp;
            tmp=nullptr;
            br_elemenata++;
            pozicija++;
        }
    }
    
    void dodajIza(const Tip &el){
        if(br_elemenata==0){
            start=new Cvor;
            start->info=el;
            start->naprijed=nullptr;
            start->nazad=nullptr;
            tren=start;
            end=tren;
            br_elemenata++;
            pozicija=0;
        }
        else if(tren==end){
            auto tmp=new Cvor;
            tmp->info=el;
            tmp->nazad=tren;
            tmp->naprijed=nullptr;
            tren->naprijed=tmp;
            end=tmp;
            tmp=nullptr;
            br_elemenata++;
            pozicija++;
        }
        else{
            auto tmp=new Cvor;
            auto pom(tren->naprijed);
            tmp->info=el;
            tmp->nazad=tren;
            tmp->naprijed=pom;
            pom->nazad=tmp;
            tren->naprijed=tmp;
            tmp=nullptr;
            br_elemenata++;
            pozicija++;
        }
    }
    
    Tip &operator[](int i)const{
        if(i<0 || i>br_elemenata-1) throw std::domain_error("Proslijedjeni index ne postoji!");
        if(i==0) return start->info;
        else if(i==br_elemenata-1) return end->info;
        else if(i==pozicija) return tren->info;
        else{
            int brojac(0);
            auto tmp(start);
            while(brojac<i){
                tmp=tmp->naprijed; brojac++;
            }
            return tmp->info;
        }
    }
    
    DvostrukaLista &operator=(const DvostrukaLista &lista){
        if(this==&lista) return *this;
        while(start!=nullptr){
            auto tmp(start->naprijed);
            delete start;
            start=tmp;
        }
        br_elemenata=lista.br_elemenata;
        pozicija=lista.pozicija;
        start=nullptr;
        end=nullptr;
        tren=nullptr;
        if(br_elemenata>0){
            start=new Cvor;
            start->info=lista.start->info;
            start->nazad=nullptr;
            end=start;
            auto tmp(lista.start->naprijed);
            while(tmp->naprijed!=nullptr){
                auto tmp2=new Cvor;
                tmp2->info=tmp->info;
                tmp2->nazad=end;
                end->naprijed=tmp2;
                tmp2->naprijed=nullptr;
                end=tmp2;
                tmp2=nullptr;
                if(tmp==lista.tren) tren=tmp;
                tmp=tmp->naprijed;
            }
        }
        return *this;
    }
    
    friend class Iterator<Tip>;
};


template<typename Tip>
class Iterator{
    private:
    const DvostrukaLista<Tip> *lista;
    /*struct Cvor{
        Tip info;
        Cvor *naprijed, *nazad;
    };*/
    typename DvostrukaLista<Tip>::Cvor* t;
    //Cvor *t;
    public:
    Iterator(const DvostrukaLista<Tip> &l):lista(&l), t(lista->start){}
    Iterator(const Lista<Tip> &l):lista((const DvostrukaLista<Tip>*) &l), t(lista->start){}
    Tip &trenutni()const{
        if(lista->brojElemenata()==0) throw std::domain_error("Lista je prazna!");
        return t->info;
    }
    bool sljedeci(){
        if(lista->brojElemenata()==0) throw std::domain_error("Lista je prazna!");
        if(t==lista->end) return false;
        t=t->naprijed;
        return true;
    }
};

template<typename Tip>
Tip dajMaksimum(const Lista<Tip> &n){
    if(n.brojElemenata()==0) throw std::domain_error("Lista je prazna!");
    Iterator<Tip> it(n);
    auto max(it.trenutni());
    if(n.brojElemenata()==1) return max;
    for(int i=0;i<n.brojElemenata();i++){
        if(it.trenutni()>max) max=it.trenutni();
        it.sljedeci();
    }
    return max;
}

void Test_Konstr1(){
    DvostrukaLista<int> l1;
    std::cout<<l1.brojElemenata()<<std::endl;
}

void Test_Konstr2(){
    DvostrukaLista<std::string> l1;
    auto l2(l1);
    std::cout<<l1.brojElemenata()<<std::endl;
}

void Test_Konstr3(){
    DvostrukaLista<int> l1;
    for(int i=0;i<10;i++)
    l1.dodajIspred(i);
    auto l2(l1);
    std::cout<<l2.brojElemenata()<<std::endl;
}

void Test_Trenutni1(){
    DvostrukaLista<double> l;
    for(double i=0.2;i<100;i+=0.2)
    l.dodajIspred(i);
    std::cout<<l.trenutni()<<std::endl;
}

void Test_Trenutni2(){
    try{
    DvostrukaLista<int> l1;
    std::cout<<l1.trenutni()<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
}

void Test_Prethodni1(){
    DvostrukaLista<double> l;
    for(double i=0.2;i<100;i+=0.2)
    l.dodajIspred(i);
    l.prethodni();
    std::cout<<l.trenutni()<<std::endl;
}

void Test_Sljedeci1(){
    DvostrukaLista<double> l;
    for(double i=0.2;i<100;i+=0.2)
    l.dodajIspred(i);
    l.sljedeci();
    std::cout<<l.trenutni()<<std::endl;
}

void Test_Pocetak1(){
    DvostrukaLista<double> l;
    for(double i=0.2;i<100;i+=0.2)
    l.dodajIspred(i);
    l.pocetak();
    std::cout<<l.trenutni()<<std::endl;
}

void Test_Kraj1(){
    DvostrukaLista<double> l;
    for(double i=0.2;i<100;i+=0.2)
    l.dodajIspred(i);
    l.kraj();
    std::cout<<l.trenutni()<<std::endl;
}

void Test_Prethodni2(){
    try{
    DvostrukaLista<int> l1;
    l1.prethodni();
    std::cout<<l1.trenutni()<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
}

void Test_Sljedeci2(){
    try{
    DvostrukaLista<int> l1;
    l1.sljedeci();
    std::cout<<l1.trenutni()<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
}

void Test_Pocetak2(){
    try{
    DvostrukaLista<int> l1;
    l1.pocetak();
    std::cout<<l1.trenutni()<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
}

void Test_Kraj2(){
    try{
    DvostrukaLista<int> l1;
    l1.pocetak();
    std::cout<<l1.trenutni()<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
}

void Test_Prethodni3(){
    DvostrukaLista<int> l;
    l.dodajIspred(100);
    if(!l.prethodni()) std::cout<<"Lista sadrzi samo jedan element!"<<std::endl;
}

void Test_Sljedeci3(){
    DvostrukaLista<int> l;
    l.dodajIspred(100);
    if(!l.sljedeci()) std::cout<<"Lista sadrzi samo jedan element!"<<std::endl;
}

void Test_Obrisi_Cijelu(){
    DvostrukaLista<int> l;
    for(int i=0;i<1000;i++)
    l.dodajIspred(i);
    for(int i=0;i<1000;i++)
    l.obrisi();
    std::cout<<l.brojElemenata()<<std::endl;
}

void Test_Obrisi_Jedan(){
    DvostrukaLista<int> l;
    for(int i=0;i<1000;i++)
    l.dodajIspred(i);
    l.obrisi();
    std::cout<<l.brojElemenata()<<std::endl;
}

void Test_Obrisi_Pola(){
    DvostrukaLista<int> l;
    for(int i=0;i<1000;i++)
    l.dodajIspred(i);
    for(int i=0;i<1000;i+=2)
    l.obrisi();
    std::cout<<l.brojElemenata()<<std::endl;
}

void Test_Obrisi_Veliku(){
    DvostrukaLista<int> l;
    for(int i=0;i<1000000;i++)
    l.dodajIspred(i);
    for(int i=0;i<1000000;i++)
    l.obrisi();
    std::cout<<l.brojElemenata()<<std::endl;
}

void Test_dodajIspred_naPraznu(){
    DvostrukaLista<std::string> l;
    l.dodajIspred("abc");
    std::cout<<l.trenutni()<<std::endl;
}

void Test_dodajIza_naPraznu(){
    DvostrukaLista<std::string> l;
    l.dodajIza("def");
    std::cout<<l.trenutni()<<std::endl;
}

void Test_Zagrade1(){
    DvostrukaLista<int> l;
    for(int i=1;i<=10;i++)
    l.dodajIspred(i);
    for(int i=0;i<10;i++)
    std::cout<<l[i]<<" ";
    std::cout<<std::endl;
}

void Test_Zagrade2(){
    try{
    DvostrukaLista<int> l;
    for(int i=1;i<=10;i++)
    l.dodajIspred(i);
    std::cout<<l[-7]<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
}

void Test_Zagrade3(){
    try{
    DvostrukaLista<int> l;
    for(int i=1;i<=10;i++)
    l.dodajIspred(i);
    std::cout<<l[10]<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
}

void Test_Dodjela(){
    DvostrukaLista<double> l1;
    for(double i=0;i<=1000;i+=0.2)
    l1.dodajIspred(i);
    DvostrukaLista<double> l2;
    l2=l1;
    std::cout<<l2.brojElemenata()<<std::endl;
}

void Test_dajMaximum1(){
    try{
        DvostrukaLista<std::string> l;
        std::string s(dajMaksimum(l));
        std::cout<<s<<std::endl;
    }catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
}

void Test_dajMaximum2(){
    DvostrukaLista<std::string> l;
    l.dodajIspred("Ovo je jedini element :)");
    std::string s(dajMaksimum(l));
    std::cout<<s<<std::endl;
}

void Test_dajMaximum3(){
    DvostrukaLista<int> l;
    for(int i=1;i<=100;i+=2)
    l.dodajIspred(i);
    for(int i=0;i<=100;i+=2)
    l.dodajIza(i);
    int najveci(dajMaksimum(l));
    std::cout<<najveci<<std::endl;
}

void Test_dajMaximum_izVelike(){
    DvostrukaLista<int> l;
    for(int i=0;i<=1000000;i++)
    l.dodajIza(i);
    int najveci(dajMaksimum(l));
    std::cout<<najveci<<std::endl;
}

int main() {
    Test_Konstr1();
    Test_Konstr2();
    Test_Konstr3();
    Test_Trenutni1();
    Test_Trenutni2();
    Test_Prethodni1();
    Test_Sljedeci1();
    Test_Pocetak1();
    Test_Kraj1();
    Test_Prethodni2();
    Test_Sljedeci2();
    Test_Pocetak2();
    Test_Kraj2();
    Test_Prethodni3();
    Test_Sljedeci3();
    Test_Obrisi_Cijelu();
    Test_Obrisi_Jedan();
    Test_Obrisi_Pola();
    Test_Obrisi_Veliku();
    Test_dodajIspred_naPraznu();
    Test_dodajIza_naPraznu();
    Test_Zagrade1();
    Test_Zagrade2();
    Test_Zagrade3();
    Test_Dodjela();
    Test_dajMaximum1();
    Test_dajMaximum2();
    Test_dajMaximum3();
    Test_dajMaximum_izVelike();
    return 0;
}
