#include <iostream>
#include <stdexcept>
#include <string>
#include <cmath>
#include <vector>

template<typename TipOznake>
class UsmjereniGraf;

template<typename TipOznake>
class Cvor;

template<typename TipOznake>
class Grana;

template<typename TipOznake>
class MatricaGraf;

template <typename TipOznake>
class GranaIterator{
    private:
    Grana<TipOznake> g;
    UsmjereniGraf<TipOznake> *graf;
    public:
    GranaIterator(Grana<TipOznake> gr, UsmjereniGraf<TipOznake> *grafik): g(gr), graf(grafik){}
    
    Grana<TipOznake> operator*(){
        return g;
    }
    
    bool operator==(const GranaIterator &iter) const{
        if(this==&iter) return true;
        if(g==iter.g) return true;
        return false;
    }
    
    bool operator!=(const GranaIterator &iter) const{
        if(this!=&iter) return true;
        if(g!=iter.g) return true;
        return false;
    }
    
    GranaIterator& operator++(){
        auto dolazni(g.dajDolazniCvor());
        int prva(dolazni.dajRedniBroj());
        int druga(dolazni.dajSljedeci());
        int nadjena(graf.dajBrojGrane(prva, druga));
        g=graf->dajGranu(nadjena);
    }
    
    GranaIterator operator++(int){
        auto pom(*this);
        return pom;
    }
};

template<typename TipOznake>
class Cvor{
    private:
    int broj;
    TipOznake oznaka;
    std::vector<int> susjedi;
    UsmjereniGraf<TipOznake> *graf;
    
    friend void MatricaGraf<TipOznake>::postaviOznakuCvora(int brc, TipOznake oznaka);
    public:
    Cvor(){}
    
    Cvor(UsmjereniGraf<TipOznake> *g, int brc): graf(g), broj(brc){}
    
    TipOznake dajOznaku () const {
        return oznaka;
    }
    
    void postaviOznaku(TipOznake o){
        oznaka=o;
        graf->postaviOznakuCvora(broj, oznaka);
    }
    
    int dajRedniBroj() const {
        return broj;
    }
    
    int dajSljedeci(){
        return susjedi[broj+1];
    }
    
    void povezi(int c){
        susjedi.push_back(c);
    }
};

template<typename TipOznake>
class Grana{
    private:
    float tezina;
    Cvor<TipOznake> polazni, dolazni;
    TipOznake oznaka;
    UsmjereniGraf<TipOznake> *graf;
    
    friend void MatricaGraf<TipOznake>::postaviTezinuGrane(int prvi, int drugi, float tezina=0);
    friend void MatricaGraf<TipOznake>::postaviOznakuGrane(int prvi, int drugi, TipOznake oznaka);
    public:
    Grana(Cvor<TipOznake> prvi, Cvor<TipOznake> drugi, UsmjereniGraf<TipOznake> *grafik, float t=0):
    polazni(prvi), dolazni(drugi), graf(grafik), tezina(t){}
    
    float dajTezinu()const{
        return tezina;
    }
    
    void postaviTezinu(float t){
        tezina=t;
        auto prvi(polazni.dajRedniBroj()), drugi(dolazni.dajRedniBroj());
        graf->postaviTezinuGrane(prvi, drugi,t);
    }
    
    TipOznake dajOznaku()const{
        return oznaka;
    }
    
    void postaviOznaku(TipOznake o){
        oznaka=o;
        auto prvi(polazni.dajRedniBroj()), drugi(dolazni.dajRedniBroj());
        graf->postaviOznakuGrane(prvi, drugi, o);
    }
    
    Cvor<TipOznake> dajPolazniCvor()const {
        return polazni;
    }
    
    Cvor<TipOznake> dajDolazniCvor() const {
        return dolazni;
    }
    
};


template<typename TipOznake>
class UsmjereniGraf{
    public:
    UsmjereniGraf(){};
    virtual ~UsmjereniGraf(){};
    virtual int dajBrojCvorova()const=0;
    virtual void postaviBrojCvorova(int brc)=0;
    virtual void dodajGranu(int prvi, int drugi, float tezina=0)=0;
    virtual void obrisiGranu(int prvi, int drugi)=0;
    virtual void postaviTezinuGrane(int prvi, int drugi, float tezina=0)=0;
    virtual float dajTezinuGrane(int prvi, int drugi)=0;
    virtual bool postojiGrana(int prvi, int drugi)=0;
    virtual void postaviOznakuCvora(int brc, TipOznake oznaka)=0;
    virtual TipOznake dajOznakuCvora(int cvor)=0;
    virtual void postaviOznakuGrane(int prvi, int drugi, TipOznake oznaka)=0;
    virtual TipOznake dajOznakuGrane(int prvi, int drugi)=0;
    virtual Cvor<TipOznake> &dajCvor(int brc)=0;
    virtual Grana<TipOznake> &dajGranu(int prvi, int drugi)=0;
    virtual GranaIterator<TipOznake> dajGranePocetak()=0;
    virtual GranaIterator<TipOznake> dajGraneKraj()=0;
    virtual int dajBrojGrane(int prvi, int drugi)=0;
};

template<typename TipOznake>
class MatricaGraf: public UsmjereniGraf<TipOznake>{
    private:
    std::vector<Cvor<TipOznake>> cvorovi;
    std::vector<Grana<TipOznake>> grane;
    std::vector<std::vector<bool>> susjedi;
    
    public:
    MatricaGraf(int broj_cvorova){
        cvorovi.resize(broj_cvorova);
        for(int i=0; i<broj_cvorova; i++){
            Cvor<TipOznake> pom(this, i);
            cvorovi[i]=pom;
        }
        susjedi.resize(broj_cvorova);
        for(int i=0; i<broj_cvorova; i++)
            susjedi[i].resize(broj_cvorova);
    };
    
    ~MatricaGraf(){};
    
    int dajBrojCvorova()const{
        return int(cvorovi.size());
    }
    
    void postaviBrojCvorova(int brc){
        if(brc<dajBrojCvorova()) throw std::domain_error("Uneseni broj je manji od trenutnog broja cvorova");
        cvorovi.resize(brc);
    }
    
    void dodajGranu(int prvi, int drugi, float tezina=0){
        susjedi[prvi][drugi]=1;
        Grana<TipOznake> pom(Grana<TipOznake>(dajCvor(prvi), dajCvor(drugi),this,tezina));
        grane.push_back(pom);
        cvorovi[prvi].povezi(drugi);
    }
    
    void obrisiGranu(int prvi, int drugi){
        if(susjedi[prvi][drugi]==0) throw std::domain_error("Unesena grana ne postoji");
        int br(dajBrojGrane(prvi,drugi));
        susjedi[prvi][drugi]=0;
        grane.erase(grane.begin()+br);
    }
    
    void postaviTezinuGrane(int prvi, int drugi, float tezina=0){
        int br(dajBrojGrane(prvi, drugi));
        if(br==-1 || susjedi[prvi][drugi]==0) throw std::domain_error("Unesena grana ne postoji");
        grane[br].tezina=tezina;
    }
    
    float dajTezinuGrane(int prvi, int drugi){
        int br(dajBrojGrane(prvi, drugi));
        if(br==-1 || susjedi[prvi][drugi]==0) throw std::domain_error("Unesena grana ne postoji");
        return grane[br].dajTezinu();
    }
    
    bool postojiGrana(int prvi, int drugi){
        return susjedi[prvi][drugi];
    }
    
    void postaviOznakuCvora(int brc, TipOznake oznaka){
        cvorovi[brc].oznaka=oznaka;
    }
    
    TipOznake dajOznakuCvora(int cvor){
        return cvorovi[cvor].dajOznaku();
    }
    
    void postaviOznakuGrane(int prvi, int drugi, TipOznake oznaka){
        if(susjedi[prvi][drugi]==0) throw std::domain_error("Unesena grana ne postoji");
        int br(dajBrojGrane(prvi,drugi));
        grane[br].oznaka=oznaka;
    }
    
    TipOznake dajOznakuGrane(int prvi, int drugi){
        if(susjedi[prvi][drugi]==0) throw std::domain_error("Unesena grana ne postoji");
        int br(dajBrojGrane(prvi,drugi));
        return grane[br].dajOznaku();
    }
    
    Cvor<TipOznake> &dajCvor(int brc){
        for(int i=0; i<int(cvorovi.size()); i++){
            if(cvorovi[i].dajRedniBroj()==brc) return cvorovi[i];
        }
        throw std::domain_error("Cvor sa unesenim brojem ne postoji");
    }
    
    Grana<TipOznake> &dajGranu(int prvi, int drugi){
        for(int i=0; i<int(grane.size()); i++){
            auto polazni(grane[i].dajPolazniCvor()) ,dolazni(grane[i].dajDolazniCvor());
            if(polazni.dajRedniBroj()==prvi && dolazni.dajRedniBroj()==drugi)
            return grane[i];
        }
    }
    
    GranaIterator<TipOznake> dajGranePocetak(){
        return GranaIterator<TipOznake>(grane[0], this);
    }
    
    GranaIterator<TipOznake> dajGraneKraj(){
        return GranaIterator<TipOznake>(grane[int(grane.size())-1], this);
    }
    
    int dajBrojGrane(int prvi, int drugi){
        for(int i=0; i<int(grane.size()); i++){
            auto polazni(grane[i].dajPolazniCvor()) ,dolazni(grane[i].dajDolazniCvor());
            if(polazni.dajRedniBroj()==prvi && dolazni.dajRedniBroj()==drugi)
            return i;
        }
        return -1;
    }
};

int main() {
    std::cout << "Pripremna Zadaca Za Vjezbu 10, Zadatak 1"<<std::endl;
    try {
    UsmjereniGraf<std::string> *g = new MatricaGraf<std::string>(5);
  g->dodajGranu(0, 1, 100);
  g->dodajGranu(1, 2, 200);
  g->dodajGranu(2, 3, 300);
  g->dodajGranu(3, 4, 400);
  g->dodajGranu(4, 5, 500);
  g->postaviOznakuGrane(2, 3, "Nejira");
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      if (g->postojiGrana(i,j))
        std::cout << "(" << i << "," << j << ") -> '" << g->dajOznakuGrane(i, j) << "'; "<<std::endl;
  delete g;
 } catch (std::domain_error izuzetak) {
    std::cout << izuzetak.what() << std::endl;
 }

    return 0;
}
