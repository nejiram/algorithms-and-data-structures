/*#include <iostream>
int main() {
    std::cout << "Tutorijal 1, Zadatak 1";
    return 0;
}
*/
#include <iostream>
#include <ctime>
#include <stdexcept>

bool Paran(long long n){
    if(n%2==0) return true;
    return false;
}

bool Prost(long long n){
    for(int i=2; i<n;i++){
        if(n%i==0) return false;
    }
    return true;
}

void Goldbach(long long n, int &p, int &q){
    int pom;
    if(!Paran(n) || n<=2) throw std::domain_error("Pogresna vrijednost!");
    for(int i=2;i<n;i++){
        if(Prost(i)){
            pom=n-i;
            if(Prost(pom)){
                p=i;
                q=pom;
                break;
            }
        }
    }
}
int main(){
    try{
        long long n;
        std::cout<<"Unesite n: ";
        std::cin>>n;
        int p,q;
        clock_t vrijeme1 = clock(); 
        Goldbach(n,p,q); 
        clock_t vrijeme2 = clock(); 
        int ukvrijeme = (vrijeme2 - vrijeme1) / (CLOCKS_PER_SEC / 1000); 
        std::cout << "Vrijeme izvrsenja: " << ukvrijeme << " ms." << std::endl;
        std::cout<<"p="<<p<<", q="<<q<<std::endl;
    } catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what();
    }
    return 0;
}