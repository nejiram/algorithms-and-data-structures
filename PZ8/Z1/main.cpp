#include <iostream>
#include <stdexcept>
#include <iomanip>
#include <ctime>

template<typename TipKljuca, typename TipVrijednosti>
class Mapa{
    public:
    Mapa(){};
    virtual ~Mapa(){};
    virtual TipVrijednosti operator[](const TipKljuca &k)const=0;
    virtual TipVrijednosti &operator[](const TipKljuca &k)=0;
    virtual int brojElemenata() const=0;
    virtual void obrisi()=0;
    virtual void obrisi(const TipKljuca &k)=0;
};

template<typename TipKljuca, typename TipVrijednosti>
class NizMapa: public Mapa<TipKljuca, TipVrijednosti>{
    private:
    std::pair<TipKljuca, TipVrijednosti> *niz;
    int br_elemenata, kapacitet;
    public:
    ~NizMapa(){
        delete[] niz;
        niz=nullptr;
    }
    
    NizMapa():br_elemenata(0), kapacitet(10) {
        niz=new std::pair<TipKljuca, TipVrijednosti> [10];
    }
    
    NizMapa(const NizMapa &m){
        br_elemenata=m.br_elemenata;
        kapacitet=m.kapacitet;
        niz=new std::pair<TipKljuca, TipVrijednosti> [kapacitet];
        
        for(int i=0; i<br_elemenata; i++)
            niz[i]=m.niz[i];
    }
    
    NizMapa &operator=(const NizMapa &m){
        if(this==&m) return *this;
        
        if(br_elemenata!=0){
        delete[] niz;
        }
        br_elemenata=m.br_elemenata;
        kapacitet=m.kapacitet;
        niz=new std::pair<TipKljuca, TipVrijednosti> [kapacitet];
        
        for(int i=0; i<br_elemenata; i++)
            niz[i]=m.niz[i];
            
        return *this;
    }
    
    TipVrijednosti operator [](const TipKljuca &k) const{
        for(int i=0;i<br_elemenata;i++){
            if(niz[i].first==k) return niz[i].second;
        }
        return TipVrijednosti();
    }
    
    TipVrijednosti &operator[](const TipKljuca &k){
        
            for(int i=0; i<br_elemenata; i++){
                if(niz[i].first==k) return niz[i].second;
            }
            if(kapacitet>br_elemenata){
                niz[br_elemenata++].first=k;
                niz[br_elemenata-1].second=TipVrijednosti();
            }
            else{
                kapacitet*=2;
                std::pair<TipKljuca, TipVrijednosti> *tmp;
                tmp=niz;
                //delete[] niz;
                niz=new std::pair<TipKljuca, TipVrijednosti>[kapacitet];
                for(int i=0; i<br_elemenata; i++){
                    niz[i]=tmp[i];
                }
                delete[] tmp;
                tmp=nullptr;
                niz[br_elemenata++].first=k;
                niz[br_elemenata-1].second=TipVrijednosti();
            }
            return niz[br_elemenata-1].second;
    }
    
    int brojElemenata() const{
        return br_elemenata;
    }
    
    void obrisi(){
        delete[] niz;
        niz=new std::pair<TipKljuca, TipVrijednosti>[kapacitet];
        br_elemenata=0;
    }
    
    void obrisi(const TipKljuca &k){
        bool ima(false);
        for(int i=0; i<br_elemenata; i++){
            if(niz[i].first==k) ima=true;
        }
        if(!ima) throw std::domain_error("Ne postoji par sa unesenim kljucem");
        for(int i=0; i<br_elemenata; i++){
            if(niz[i].first==k){
                for(int j=i;j<br_elemenata-1; j++){
                    niz[j]=niz[j+1];
                }
                br_elemenata--;
                break;
            }
        }
    }
};

template<typename TipKljuca, typename TipVrijednosti>
class BinStabloMapa: public Mapa<TipKljuca, TipVrijednosti>{
    private:
    int br_elemenata;
    struct Cvor{
        TipKljuca kljuc;
        TipVrijednosti info;
        Cvor* lijevo;
        Cvor *desno;
        Cvor *roditelj;
    };
    Cvor *korijen;
  
    Cvor* &pomocnaZaKonstruktore(Cvor* c, Cvor* &novi, Cvor* roditelj){
        if(c==nullptr) novi=nullptr;
        else{
            novi=new Cvor;
            novi->kljuc=c->kljuc;
            novi->info=c->info;
            novi->roditelj=roditelj;
            pomocnaZaKonstruktore(c->lijevo,novi->lijevo ,novi);
            pomocnaZaKonstruktore(c->desno,novi->desno ,novi);
            if(novi->roditelj==nullptr) return novi;
        }
    }
    
    void pomocnaZaBrisanjeSvega(Cvor* r){
        if(r==nullptr) return;
        else{
            if(r->lijevo!=nullptr) pomocnaZaBrisanjeSvega(r->lijevo);
            if(r->desno!=nullptr) pomocnaZaBrisanjeSvega(r->desno);
            if(r!=nullptr) delete r;
        }
    }
    
    void pomocnaZaBrisanje(Cvor *tmp){
        if(tmp==nullptr) return;
        if(tmp->lijevo==nullptr && tmp->desno==nullptr){
                if(tmp==tmp->roditelj->lijevo) tmp->roditelj->lijevo=nullptr;
                else tmp->roditelj->desno=nullptr;
                delete tmp;
                br_elemenata--;
            }
            else if(tmp->lijevo!=nullptr && tmp->desno!=nullptr){
                Cvor *pom(tmp->lijevo);
                while(pom->desno!=nullptr) pom=pom->desno;
                tmp->kljuc=pom->kljuc;
                tmp->info=pom->info;
                pomocnaZaBrisanje(pom);
            }
            else{
                Cvor* pom;
                if(tmp->lijevo==nullptr) pom=tmp->desno;
                else pom=tmp->lijevo;
                if(tmp->roditelj==nullptr){
                    pom->roditelj=tmp->roditelj;
                    delete tmp;
                    korijen=pom;
                    br_elemenata--;
                }
                else if(tmp==tmp->roditelj->lijevo) {
                    tmp->roditelj->lijevo=pom;
                    pom->roditelj=tmp->roditelj;
                    delete tmp;
                    br_elemenata--;
                }
                else {
                tmp->roditelj->desno=pom;
                pom->roditelj=tmp->roditelj;
                delete tmp;
                br_elemenata--;
                }
            }
    }
    
    Cvor* pomocnaZaTrazenje(Cvor *korijen, const TipKljuca &k){
        if(korijen==nullptr || korijen->kljuc==k) return korijen;
        else if(k<korijen->kljuc) return pomocnaZaTrazenje(korijen->lijevo, k);
        return pomocnaZaTrazenje(korijen->desno, k);
    }
    
    TipVrijednosti &pomocnaZaOperator(Cvor *korijen, const TipKljuca &k){
        if(korijen->kljuc==k) return korijen->info;
        else if(korijen==nullptr){
            korijen->roditelj=nullptr;
            korijen->desno=nullptr;
            korijen->lijevo=nullptr;
            korijen->kljuc=k;
            korijen->info=TipVrijednosti();
            br_elemenata++;
            return korijen->info;
        }
        else if(korijen->kljuc<k && korijen->desno==nullptr){
            korijen->desno=new Cvor;
            korijen->desno->kljuc=k;
            korijen->desno->desno=nullptr;
            korijen->desno->lijevo=nullptr;
            korijen->desno->roditelj=korijen;
            korijen->desno->info=TipVrijednosti();
            br_elemenata++;
            return korijen->desno->info;
        }
        else if(korijen->kljuc>k && korijen->lijevo==nullptr){
            korijen->lijevo=new Cvor;
            korijen->lijevo->kljuc=k;
            korijen->lijevo->desno=nullptr;
            korijen->lijevo->lijevo=nullptr;
            korijen->lijevo->roditelj=korijen;
            korijen->lijevo->info=TipVrijednosti();
            br_elemenata++;
            return korijen->lijevo->info;
        }
        else if(korijen->kljuc<k && korijen->desno!=nullptr) return pomocnaZaOperator(korijen->desno, k);
        return pomocnaZaOperator(korijen->lijevo,k);
    }
    
    Cvor* maxBst(Cvor *korijen){
        if(korijen->desno==nullptr) return korijen;
        return maxBst(korijen->desno);
    }
    
    TipVrijednosti pomocnaZaKljuc(Cvor *korijen, const TipKljuca &k) const{
        if(korijen==nullptr) return TipVrijednosti();
        if(korijen->kljuc==k) return korijen->info;
        if(korijen->kljuc<k) return pomocnaZaKljuc(korijen->desno, k);
        return pomocnaZaKljuc(korijen->lijevo, k);
    }
        
    public:
    ~BinStabloMapa(){
        pomocnaZaBrisanjeSvega(korijen);
    }
    
    BinStabloMapa(): br_elemenata(0), korijen(nullptr) {}
    
    BinStabloMapa(const BinStabloMapa &m): br_elemenata(m.br_elemenata) {
        br_elemenata=m.br_elemenata;
        if(br_elemenata!=0){ 
            korijen=pomocnaZaKonstruktore(m.korijen, korijen, nullptr);
        }
        else {
            korijen=nullptr;
        }
    }
    
    BinStabloMapa  &operator=(const BinStabloMapa<TipKljuca, TipVrijednosti> &m){
        if(this==&m) return *this;
        if(br_elemenata!=0) obrisi();
        br_elemenata=m.br_elemenata;
        if(br_elemenata!=0)
        korijen=pomocnaZaKonstruktore(m.korijen, korijen, nullptr);
        else korijen=nullptr;
        return *this;
    }
    
    TipVrijednosti operator[](const TipKljuca &k)const{
        return pomocnaZaKljuc(korijen, k);
    }
    
    TipVrijednosti &operator[](const TipKljuca &k){
        if(br_elemenata==0){
            korijen=new Cvor;
            korijen->kljuc=k;
            korijen->roditelj=nullptr;
            korijen->desno=nullptr;
            korijen->lijevo=nullptr;
            br_elemenata++;
            korijen->info=TipVrijednosti();
            return korijen->info;
        }
        else if(korijen->kljuc==k) return korijen->info;
        return pomocnaZaOperator(korijen, k);
    }
    
    int brojElemenata() const {
        return br_elemenata;
    }
    
    void obrisi(){
        pomocnaZaBrisanjeSvega(korijen);
        korijen=nullptr;
        br_elemenata=0;
    }
    
    void obrisi(const TipKljuca &k){
        Cvor* tmp(pomocnaZaTrazenje(korijen, k));
        if(tmp==nullptr) return;
        pomocnaZaBrisanje(tmp);
    }
};


int main() {
    std::cout << "Pripremna Zadaca Za Vjezbu 8, Zadatak 1"<<std::endl;
 
NizMapa<int, std::string> niz;
for(int i=0; i<5000; i++)
    niz[i]="";    

BinStabloMapa<int, std::string> stablo;
for(int i=0; i<5000; i++)
    stablo[i]="";    

clock_t vrijemeDodavanjaNiz1=clock();
niz[10000]="Pristup elementu niza";
clock_t vrijemeDodavanjaNiz2=clock();
int ukVrijemeDodavanjaNiz=(vrijemeDodavanjaNiz2-vrijemeDodavanjaNiz1)/(CLOCKS_PER_SEC / 1000);

clock_t vrijemeDodavanjaStablo1=clock();
stablo[10000]="Pristup elementu stabla";
clock_t vrijemeDodavanjaStablo2=clock();
int ukVrijemeDodavanjaStablo=(vrijemeDodavanjaStablo2-vrijemeDodavanjaStablo1)/(CLOCKS_PER_SEC / 1000);

std::cout<<"Vrijeme dodavanja novog elementa u niz je: "<<ukVrijemeDodavanjaNiz<<"ms, a u binarno stablo: "<<ukVrijemeDodavanjaStablo<<"ms."<<std::endl;

clock_t vrijemePristupaNiz1=clock();
std::cout<<niz[333]<<std::endl;
clock_t vrijemePristupaNiz2=clock();
int ukVrijemePristupaNiz=(vrijemePristupaNiz2-vrijemePristupaNiz1)/(CLOCKS_PER_SEC / 1000);

clock_t vrijemePristupaStablo1=clock();
std::cout<<stablo[333]<<std::endl;
clock_t vrijemePristupaStablo2=clock();
int ukVrijemePristupaStablo=(vrijemePristupaStablo2-vrijemePristupaStablo1)/(CLOCKS_PER_SEC / 1000);

std::cout<<"Vrijeme pristupa elementu niza je: "<<ukVrijemePristupaNiz<<"ms, a binarnog stabla: "<<ukVrijemePristupaStablo<<"ms."<<std::endl;


/* Mozemo zakljuciti da je vrijeme dodavanja novog elementa u binarno stablo nesto krace nego dodavenje novog elementa u niz,
dok je pristup elementima otprilike iste brzine*/
    return 0;
}
