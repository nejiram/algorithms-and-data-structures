#include <iostream>
#include <stdexcept>

template<typename Tip>
class Lista{
    public:
  Lista(){};
  virtual ~Lista(){};
  Lista(const Lista &l);
  virtual int brojElemenata()const=0;
  virtual Tip &trenutni()=0;
  virtual Tip trenutni() const=0;
  virtual bool prethodni()=0;
  virtual bool sljedeci()=0;
  virtual void pocetak()=0;
  virtual void kraj()=0;
  virtual void obrisi()=0;
  virtual void dodajIspred(const Tip &el)=0;
  virtual void dodajIza(const Tip &el)=0;
  virtual Tip &operator[](int i)=0;
  virtual Tip operator[](int i)const=0;
};

template<typename Tip>
class NizLista: public Lista<Tip>{
    private:
    int br_elemenata, pozicija, kapacitet;
    Tip *lista;
    
    public:
    NizLista():br_elemenata(0),pozicija(0),kapacitet(10){
        lista=new Tip[10];
    }
    
    NizLista(const NizLista &niz){
        br_elemenata=niz.br_elemenata;
        pozicija=niz.pozicija;
        kapacitet=niz.kapacitet;
        lista=new Tip[kapacitet];
        for(int i=0;i<niz.br_elemenata;i++)
        lista[i]=niz.lista[i];
    }
    
    NizLista &operator=(const NizLista &niz){
        if(this==&niz) return *this;
        kapacitet=niz.kapacitet;
        br_elemenata=niz.br_elemenata;
        pozicija=niz.pozicija;
        delete[] lista;
        lista=new Tip[kapacitet];
        for(int i=0;i<br_elemenata;i++)
            lista[i]=niz.lista[i];
            return *this;
    }
    
    int brojElemenata()const{return br_elemenata; }
    
    Tip &trenutni(){
        if(br_elemenata==0) throw std::domain_error("Lista je prazna!");
        return lista[pozicija];
    }
    
    Tip trenutni() const {
        if(br_elemenata==0) throw std::domain_error("Lista je prazna!");
        return lista[pozicija];
    }
    
    bool prethodni(){
        if(br_elemenata==0) throw std::domain_error("Lista je prazna");
        if(pozicija==0) return false;
        else{
            pozicija--;
            return true;
        }
    }
    
    bool sljedeci(){
        if(br_elemenata==0) throw std::domain_error("Lista je prazna!");
        if(pozicija>=br_elemenata-1) return false;
        else{
            pozicija++;
            return true;
        }
    }
    
    void pocetak(){
        if(br_elemenata==0) throw std::domain_error("Lista je prazna!");
        pozicija=0;
    }
    
    void kraj(){
        if(br_elemenata==0) throw std::domain_error("Lista je prazna!");
        pozicija=br_elemenata-1;
    }
    
    void obrisi(){
        if(br_elemenata==0) throw std::domain_error("Lista je prazna!");
        for(int i=pozicija;i<br_elemenata-1;i++)
        lista[i]=lista[i+1];
        if(pozicija==br_elemenata-1) pozicija=br_elemenata-2;
        br_elemenata--;
    }
    
    void dodajIza(const Tip &el){
        if(br_elemenata==0){
            br_elemenata++;
            pozicija=0;
            lista[pozicija]=el;
        }
        else{
            if(br_elemenata+1<=kapacitet){
                for(int i=br_elemenata;i>=pozicija+1;i--)
                    lista[i]=lista[i-1];
                lista[pozicija+1]=el;
                br_elemenata++;
            }
            else{
                kapacitet*=2;
                Tip *tmp=new Tip[kapacitet];
                for(int i=0;i<pozicija;i++)
                    tmp[i]=lista[i];
                tmp[pozicija+1]=el;
                for(int i=pozicija+1;i<br_elemenata;i++)
                    tmp[i+1]=lista[i];
                delete[] lista;
                lista=tmp;
                tmp=nullptr;
                br_elemenata++;
            }
        }
    }
    
    void dodajIspred(const Tip &el){
        if(br_elemenata==0){
            br_elemenata++;
            pozicija=0;
            lista[pozicija]=el;
        }
        else{
            if(br_elemenata+1<=kapacitet){
                for(int i=br_elemenata;i>pozicija;i--)
                    lista[i]=lista[i-1];
                lista[pozicija]=el;
                br_elemenata++;
                pozicija++;
            }
            else{
                kapacitet*=2;
                Tip *tmp=new Tip[kapacitet];
                for(int i=0;i<pozicija;i++)
                    tmp[i]=lista[i];
                tmp[pozicija]=el;
                for(int i=pozicija;i<br_elemenata;i++)
                    tmp[i+1]=lista[i];
                delete[] lista;
                lista=tmp;
                tmp=nullptr;
                br_elemenata++;
            }
        }
    }
    
    ~NizLista(){
        delete[] lista;
    }
    
    Tip &operator[](int i){
        if(i<0 || i>br_elemenata-1) throw std::domain_error("Proslijedjeni index ne postoji!");
        return lista[i];
    }
    
    Tip operator[](int i)const{
        if(i<0 || i>br_elemenata-1) throw std::domain_error("Proslijedjeni index ne postoji!");
        return lista[i];
    }
    
};

//testne funkcije
void TestNiz1(){
    NizLista<int> niz;
    for(int i=0;i<10;i++)
        niz.dodajIspred(i+1);
    NizLista<int> niz2(niz);
    NizLista<int> niz3;
    niz3=niz;
    std::cout<<niz2.brojElemenata()<<std::endl;
    std::cout<<niz3.brojElemenata()<<std::endl;
}

void TestNiz2(){
    NizLista<int> niz;
    for(int i=1;i<=1000;i++)
    niz.dodajIspred(i);
    std::cout<<niz.brojElemenata()<<std::endl;
    for(int i=1;i<=500;i++)
    niz.obrisi();
    std::cout<<niz.brojElemenata()<<std::endl;
}

void TestNiz3(){
    NizLista<int> niz;
    for(int i=0;i<10;i++)
        niz.dodajIspred(i+1);
    for(int i=10;i>0;i++)
    niz.dodajIza(i);
    std::cout<<niz.brojElemenata()<<std::endl;
}


template<typename Tip>
class JednostrukaLista:public Lista<Tip>{
  private:
  struct Cvor{
      Tip info;
      Cvor *veza;
  };
  Cvor *start,*end,*tren;
  int br_elemenata;
  public:
  
  JednostrukaLista():start(nullptr),end(nullptr),tren(nullptr),br_elemenata(0){};
  
  int brojElemenata()const{
      return br_elemenata;
  }
  
  Tip &trenutni(){
      return tren->info;
  }
  
  Tip trenutni() const {
        if(br_elemenata==0) throw std::domain_error("Lista je prazna!");
        return tren->info;
    }
    
  bool prethodni(){
      if(tren==start) return false;
      Cvor *p(start);
      while(p->veza!=tren) p=p->veza;
      tren=p;
      p=nullptr;
      return true;
  }
  
  bool sljedeci(){
      if(tren==end) return false;
      tren=tren->veza;
      return true;
  }
  
  void pocetak(){
      tren=start;
  }
  
  void kraj(){
      tren=end;
  }
  
  void obrisi(){
      if(br_elemenata==1){
          delete tren;
          tren=nullptr;
          start=nullptr;
          end=nullptr;
          br_elemenata=0;
      }
      else if(tren==end){
          Cvor *p(start);
          while(p->veza!=tren) p=p->veza;
          delete tren;
          end=p; p=nullptr; tren=end; br_elemenata--;
          tren->veza=nullptr;
      }
      else if(tren==start){
          start=tren->veza;
          delete tren;
          tren=start;
          br_elemenata--;
      }
      else{
          Cvor *p(start);
          Cvor *s(tren->veza);
          while(p->veza!=tren) p=p->veza;
          delete tren;
          tren=s;
          p->veza=tren;
          s=nullptr; p=nullptr;
          br_elemenata--;
      }
  }
  
  void dodajIspred(const Tip &el){
      if(br_elemenata==0){
          start=new Cvor;
          end=start;
          tren=start;
          start->veza=nullptr;
          start->info=el;
          br_elemenata++;
      }
      else if(tren==start){
          Cvor *p=new Cvor;
          p->info=el;
          p->veza=tren;
          start=p;
          p=nullptr;
          br_elemenata++;
      }
      else{
          Cvor *novi=new Cvor;
          novi->info=el;
          novi->veza=tren;
          Cvor *p(start);
          while(p->veza!=tren) p=p->veza;
          p->veza=novi;
          br_elemenata++;
          p=nullptr; novi=nullptr;
      }
  }
  
  void dodajIza(const Tip &el){
      if(br_elemenata==0){
          start=new Cvor;
          end=start;
          tren=start;
          start->veza=nullptr;
          start->info=el;
          br_elemenata++;
      }
      else if(tren==end){
          Cvor *p=new Cvor;
          p->info=el;
          p->veza=nullptr;
          end->veza=p;
          end=p; p=nullptr;
          br_elemenata++;
      }
      else{
          Cvor *novi=new Cvor;
          Cvor *p(tren->veza);
          novi->info=el;
          novi->veza=p;
          tren->veza=novi;
          br_elemenata++;
          novi=nullptr;
          p=nullptr;
      }
  }
  
  Tip &operator[](int i){
      if(i<0 || i>br_elemenata-1) throw std::domain_error("Proslijedjeni index ne postoji!");
      int brojac(0);
      Cvor *p(start);
      while(brojac<i){
          p=p->veza; brojac++;
      }
      return p->info;
  }
  
  Tip operator[](int i)const{
      if(i<0 || i>br_elemenata-1) throw std::domain_error("Proslijedjeni index ne postoji!");
      int brojac(0);
      Cvor *p(start);
      while(brojac<i){
          p=p->veza; brojac++;
      }
      return p->info;
  }
  
  JednostrukaLista(const JednostrukaLista &lista){
      br_elemenata=0;
      Cvor *p=lista.start;
      while(p->veza!=nullptr){
          this->dodajIza(p->info);
          this->sljedeci();
          p=p->veza;
      }
      this->dodajIza(p->info);
    }
  
  JednostrukaLista &operator=(const JednostrukaLista &lista){
      if(this==&lista) return *this;
      if(start!=nullptr){
          while(start!=nullptr){
              Cvor *p=start->veza;
              delete start;
              start=p;
          }
      }
      br_elemenata=0;
      Cvor *p=lista.start;
      while(p->veza!=nullptr){
          this->dodajIza(p->info);
          p=p->veza;
      }
      this->dodajIza(p->info);
      return *this;
  }
  
  ~JednostrukaLista(){
      if(start!=nullptr){
          while(start!=nullptr){
              Cvor *p=start->veza;
              delete start;
              start=p;
          }
      }
  }
};

//testne funkcije 
void TestLista1(){
    JednostrukaLista<int> l;
    for (int i=0; i<10; i++)
	l.dodajIspred(i+1);
    l.pocetak();
    std::cout<<l.trenutni()<<std::endl;
}

void TestLista2(){
    JednostrukaLista<int> l;
    for (int i=0; i<10; i++)
	l.dodajIspred(i+1);
    l.kraj();
    std::cout<<l.trenutni()<<std::endl;
}

void TestLista3(){
    JednostrukaLista<int> l;
    for (int i=0; i<10; i++)
	l.dodajIspred(i+1);
    l.fija();
    std::cout<<std::endl;
    for(int i=10;i>0;i--)
    l.dodajIza(i);
    l.fija();
}


int main(){
    NizLista<int> niz;
    for (int i(1); i<=5; i++)
	    niz.dodajIspred(i);
    const NizLista<int>& konst(niz);
    std::cout << konst.brojElemenata() << " " << konst.trenutni();
    std::cout << " " << konst[0] << std::endl;
    niz.trenutni() = 15;
    niz[0] = 20;
    std::cout << konst.trenutni() << " " << konst[0] << std::endl;
    std::cout<<std::endl;
    
    TestNiz1();
    TestNiz2();
    TestNiz3();
    std::cout<<std::endl;
    
    TestLista1();
    TestLista2();
    TestLista3();
    return 0;
}