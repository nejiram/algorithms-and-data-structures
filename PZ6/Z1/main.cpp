#include <iostream>
#include <fstream>
#include <ctime>
#include <functional>

template<typename Tip>
void bubble_sort(Tip* niz, int vel){
    for(int i=vel-1;i>=0;i--){
        for(int j=1;j<=i;j++){
            if(niz[j-1]>niz[j]){
              Tip tmp(niz[j-1]);
              niz[j-1]=niz[j]; 
              niz[j]=tmp;
            } 
        }
    }
}

template<typename Tip>
void selection_sort(Tip* niz, int vel){
    for(int i=0;i<vel;i++){
        Tip min(niz[i]);
        int pmin(i);
        for(int j=i+1;j<=vel-1;j++){
            if(niz[j]<min){
                min=niz[j];
                pmin=j;
            }
        }
        niz[pmin]=niz[i];
        niz[i]=min;
    }
}

template<typename Tip>
int particija(Tip* niz, int prvi, int zadnji){
    auto pivot(niz[prvi]);
    auto p(prvi+1);
    while (p<=zadnji && niz[p] < pivot) {
        p=p+1;
    } 
    for(int i=p+1;i<=zadnji;i++){
        if(niz[i]<pivot) {
            auto tmp(niz[i]);
            niz[i]=niz[p];
            niz[p]=tmp;
            p=p+1;
        }
    }
    auto tmp(niz[prvi]);
    niz[prvi]=niz[p-1];
    niz[p-1]=tmp;
    return p-1;
}

template<typename Tip>
void quick_sort_pomocna(Tip* niz, int prvi, int zadnji){
    if(prvi<zadnji){
        auto j(particija(niz,prvi,zadnji));
        quick_sort_pomocna(niz,prvi,j-1);
        quick_sort_pomocna(niz,j+1,zadnji);
    }
}

template<typename Tip>
void quick_sort(Tip* niz, int vel){
    quick_sort_pomocna(niz,0,vel-1);
}

template<typename Tip>
void merge(Tip *niz, int l, int p, int q, int u){
    int i(0),j(q-l), k(l);
    auto tmp(new Tip[u-l+1]);
    for(int m=0;m<u-l+1;m++){
        tmp[m]=niz[l+m];
    }
    while (i<=p-l && j<=u-l) {
        if(tmp[i]<tmp[j]){
            niz[k]=tmp[i];
            i++;
        }
        else{
            niz[k]=tmp[j];
            j++;
        }
        k++;
    }
    while (i<=p-l) {
        niz[k]=tmp[i];
        k++;
        i++;
    }
    while (j<=u-l) {
        niz[k]=tmp[j];
        k++;
        j++;
    }
    delete[] tmp;
}

template<typename Tip>
void merge_sort_pomocna(Tip* niz, int l, int u){
    if(u>l){
        int p((l+u-1)/2);
        int q(p+1);
        merge_sort_pomocna(niz,l,p);
        merge_sort_pomocna(niz,q,u);
        merge(niz,l,p,q,u);
    }
}

template<typename Tip>
void merge_sort(Tip* niz, int vel){
    merge_sort_pomocna(niz,0,vel-1);
}

void ucitaj(std::string filename, int*& niz, int &vel){
    std::ifstream ulazni_tok(filename);
    niz=new int[vel];
    for(int i=0;i<vel;i++){
        if(!ulazni_tok) break;
        ulazni_tok>>niz[i];
    }
}

void generisi(std::string filename, int vel){
    std::ofstream izlazni_tok(filename);
    for(int i=0;i<vel;i++){
        if(!izlazni_tok) break;
        izlazni_tok<<rand();
    }
    izlazni_tok.close();
}

template<typename Tip>
bool ispravan(Tip* niz, int vel){
    for(int i=0;i<vel-1;i++)
        if(niz[i]>niz[i+1])
            return false;
    return true;
}

template<typename Tip>
int sortirajIdajVrijeme(void fja(Tip* niz, int vel), Tip *niz, int vel){
    clock_t vrijeme1 = clock();
    fja(niz,vel);
    clock_t vrijeme2 = clock();
    int ukvrijeme = (vrijeme2 - vrijeme1) / (CLOCKS_PER_SEC / 1000);
    return ukvrijeme;
}

int main() {
    int *niz,vel, broj,vrijeme;
    ucitaj("nizBrojeva.txt",niz,vel);
    std::cout<<"Unesite velicinu niza: "<<std::endl;
    std::cin>>vel;
    std::cout<<"Unesite elemente niza: "<<std::endl;
    for(int i=0;i<vel;i++) std::cin>>niz[i];
    std::cout<<"Koji algoritam sortiranja zelite koristiti: \n 1. BUBBLE SORT \n 2. SELECTION SORT \n 3. QUICK SORT \n 4. MERGE SORT \n ";
    do{
        std::cin>>broj;
        if(broj<1 || broj>4) std::cout<<"Pogresan unos. Unesite ponovo!"<<std::endl;
        else break;
    }while(1);
    if(broj==1){
        vrijeme=sortirajIdajVrijeme(bubble_sort,niz,vel);
    }
    else if(broj==2){
        vrijeme=sortirajIdajVrijeme(selection_sort,niz,vel);
    }
    else if(broj==3){
        vrijeme=sortirajIdajVrijeme(quick_sort,niz,vel);
    }
    else if(broj==4){
        vrijeme=sortirajIdajVrijeme(merge_sort,niz,vel);
    }
    
    std::cout<<"Funkcija se izvrsavala "<<vrijeme<<"ms."<<std::endl;
    if(ispravan(niz,vel)) std::cout<<"Niz je sortiran ispravno."<<std::endl;
    else std::cout<<"Niz nije sortiran ispravno."<<std::endl;
    
    for(int i=0;i<vel;i++) std::cout<<niz[i]<<" ";
    
    std::ofstream izlazni_tok;
    for(int i=0;i<vel;i++) izlazni_tok<<niz[i]<<" ";
    izlazni_tok.close();
    delete []niz;
    
}
