#include <iostream>
#include <stdexcept>

template<typename Tip>
class DvostraniRed{
    private:
    int br_elemenata;
    struct Cvor{
        Tip info;
        Cvor *naprijed, *nazad;
    };
    Cvor *pocetak, *kraj;
    //celo==pocetak, vrh==kraj
    public:
    DvostraniRed():br_elemenata(0),pocetak(nullptr),kraj(nullptr){};
    
    DvostraniRed(const DvostraniRed &r){
        br_elemenata=r.br_elemenata;
        if(br_elemenata==0){
            pocetak=nullptr;
            kraj=nullptr;
        }
        else if(br_elemenata>0){
            pocetak=new Cvor;
            pocetak->info=r.pocetak->info;
            pocetak->nazad=nullptr;
            pocetak->naprijed=nullptr;
            kraj=pocetak;
            auto tmp(r.pocetak->naprijed);
            while(tmp!=nullptr){
                auto pom=new Cvor;
                pom->info=tmp->info;
                pom->nazad=kraj;
                pom->naprijed=nullptr;
                kraj->naprijed=pom;
                kraj=pom;
                pom=nullptr;
                tmp=tmp->naprijed;
                delete pom;
            }
        }
    }
    
    ~DvostraniRed(){
        if(br_elemenata!=0)
        while(pocetak!=nullptr){
            auto p(pocetak->naprijed);
            delete pocetak;
            pocetak=p;
        }
    }
    
    DvostraniRed &operator=(const DvostraniRed &r){
        if(this==&r) return *this;
        if(br_elemenata!=0)
        while(pocetak!=nullptr){
            auto tmp(pocetak->naprijed);
            delete pocetak;
            pocetak=tmp;
        }
        br_elemenata=r.br_elemenata;
        if(br_elemenata==0){
            pocetak=nullptr;
            kraj=nullptr;
        }
        else if(br_elemenata>0){
            pocetak=new Cvor;
            pocetak->info=r.pocetak->info;
            pocetak->nazad=nullptr;
            pocetak->naprijed=nullptr;
            kraj=pocetak;
            auto tmp(r.pocetak->naprijed);
            while(tmp!=nullptr){
                auto pom=new Cvor;
                pom->info=tmp->info;
                pom->nazad=kraj;
                pom->naprijed=nullptr;
                kraj->naprijed=pom;
                kraj=pom;
                pom=nullptr;
                tmp=tmp->naprijed;
                delete pom;
            }
        }
        return *this;
    }
    
    int brojElemenata(){
        return br_elemenata;
    }
    
    void brisi(){
        if(br_elemenata==0) throw std::domain_error("Dvostrani red je prazan!");
        while(pocetak!=0){
            auto p(pocetak->naprijed);
            delete pocetak;
            pocetak=p;
        }
        br_elemenata=0;
    }
    
    void staviNaVrh(const Tip &el){
        if(br_elemenata==0){
            kraj=new Cvor;
            kraj->info=el;
            kraj->naprijed=nullptr;
            kraj->nazad=nullptr;
            pocetak=kraj;
            br_elemenata++;
        }
        else{
            auto tmp=new Cvor;
            tmp->info=el;
            tmp->naprijed=nullptr;
            tmp->nazad=kraj;
            kraj->naprijed=tmp;
            kraj=tmp;
            br_elemenata++;
            //tmp=nullptr;
        }
    }
    
    void staviNaCelo(const Tip &el){
        if(br_elemenata==0){
            pocetak=new Cvor;
            pocetak->info=el;
            pocetak->naprijed=nullptr;
            pocetak->nazad=nullptr;
            kraj=pocetak;
            br_elemenata++;
        }
        else{
            auto tmp=new Cvor;
            tmp->info=el;
            tmp->nazad=nullptr;
            tmp->naprijed=pocetak;
            pocetak->nazad=tmp;
            pocetak=tmp;
            br_elemenata++;
        }
    }
    
    Tip skiniSaVrha(){
        if(br_elemenata==0) throw std::domain_error("Dvostruki red je prazan!");
        if(br_elemenata==1){
            Tip pom(kraj->info);
            delete kraj;
            kraj=nullptr;
            pocetak=nullptr;
            br_elemenata--;
            return pom;
        }
        else{
            auto tmp(kraj->nazad);
            Tip pom(kraj->info);
            delete kraj;
            kraj=tmp;
            kraj->naprijed=nullptr;
            br_elemenata--;
            return pom;
        }
    }
    
    Tip skiniSaCela(){
        if(br_elemenata==0) throw std::domain_error("Dvostruki red je prazan!");
        if(br_elemenata==1){
            Tip pom(pocetak->info);
            delete pocetak;
            pocetak=nullptr;
            kraj=nullptr;
            br_elemenata--;
            return pom;
        }
        else{
            auto tmp(pocetak->naprijed);
            Tip pom(pocetak->info);
            delete pocetak;
            pocetak=tmp;
            pocetak->nazad=nullptr;
            br_elemenata--;
            return pom;
        }
    }
    
    Tip &vrh(){
        if(br_elemenata==0) throw std::domain_error("Dvostrani red je prazan!");
        return kraj->info;
    }
    
    Tip &celo(){
        if(br_elemenata==0) throw std::domain_error("Dvostrani red je prazan!");
        return pocetak->info;
    }
};

void TestKonstr1(){
    DvostraniRed<std::string> s;
    std::cout<<s.brojElemenata()<<std::endl;
}

void TestKonstr2(){
    DvostraniRed<int> s1;
    for(int i=0;i<10;i++)
    s1.staviNaCelo(i);
    auto s2(s1);
    std::cout<<s2.brojElemenata()<<std::endl;
}

void TestDodjela(){
    DvostraniRed<int> s1;
    for(int i=1;i<=1000;i++)
    s1.staviNaVrh(i);
    DvostraniRed<int> s2;
    s2=s1;
    std::cout<<s2.brojElemenata()<<std::endl;
}

void TestBrisiSve(){
    DvostraniRed<int> s;
    for(int i=0;i<10000;i++)
    s.staviNaCelo(i);
    s.brisi();
    std::cout<<s.brojElemenata()<<std::endl;
}

void TestStaviNaVrh(){
    DvostraniRed<double> s;
    for(double i=0.2;i<=100;i+=0.2)
    s.staviNaVrh(i);
    std::cout<<s.vrh()<<std::endl;
}

void TestStaviNaCelo(){
    DvostraniRed<double> s;
    for(int i=100;i>=0.2;i-=0.2)
    s.staviNaCelo(i);
    std::cout<<s.celo()<<std::endl;
}

void TestSkiniSaVrha(){
    DvostraniRed<int> s;
    for(int i=1;i<=10;i++)
    s.staviNaVrh(i);
    std::cout<<s.skiniSaVrha()<<std::endl;
}

void TestSkiniSaCela(){
    DvostraniRed<int> s;
    for(int i=10;i>=1;i--)
    s.staviNaCelo(i);
    std::cout<<s.skiniSaCela()<<std::endl;
}

void TestSkiniSaVrhaSve(){
    DvostraniRed<int> s;
    for(int i=0;i<=5000;i++)
    s.staviNaVrh(i);
    for(int i=0;i<=5000;i++)
    s.skiniSaVrha();
    std::cout<<s.brojElemenata()<<std::endl;
}

void TestSkiniSaCelaSve(){
    DvostraniRed<int> s;
    for(int i=0;i<=5000;i++)
    s.staviNaCelo(i);
    for(int i=0;i<=5000;i++)
    s.skiniSaCela();
    std::cout<<s.brojElemenata()<<std::endl;
}

int main() {
    std::cout << "Zadaća 2, Zadatak 1";
    TestKonstr1();
    TestKonstr2();
    TestDodjela();
    TestBrisiSve();
    TestStaviNaVrh();
    TestStaviNaCelo();
    TestSkiniSaVrha();
    TestSkiniSaCela();
    TestSkiniSaVrhaSve();
    TestSkiniSaCelaSve();
    return 0;
}
