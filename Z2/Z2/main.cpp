#include <iostream>
#include <stdexcept>
#include <vector>



template<typename Tip>
class Stek{
    private:
    int br_elemenata;
    struct Cvor{
        Tip info;
        Cvor *veza;
    };
    Cvor *top;
    public:
    Stek():br_elemenata(0),top(nullptr){}
    ~Stek(){
        while(top!=nullptr){
            Cvor *p=top->veza;
            delete top;
            top=p;
        }
    }
    
    Stek(const Stek &s){
        br_elemenata=s.br_elemenata;
    
    if(br_elemenata!=0){
        auto pomocni(s.top);
        top=new Cvor;
        top->info=pomocni->info;
        top->veza=nullptr;
        auto pocetak(top);
        pomocni=pomocni->veza;
        while(pomocni!=nullptr){
           Cvor *p=new Cvor;
           p->info=pomocni->info;
           p->veza=nullptr;
           pocetak->veza=p;
           pocetak=p;
           p=nullptr;
           pomocni=pomocni->veza;
        } 
    } 
    else top=nullptr;
    }
    
    void brisi(){
        while(top!=nullptr){
            Cvor *p=top->veza;
            delete top;
            top=p;
        }
        br_elemenata=0;
    }
    
    void stavi(const Tip &el){
        if(top==nullptr){
            top=new Cvor;
            top->info=el;
            top->veza=nullptr;
            br_elemenata++;
        }
        else if(br_elemenata>0){
            Cvor *p=new Cvor;
            p->veza=top;
            p->info=el;
            top=p;
            p=nullptr;
            br_elemenata++;
        }
    }
    
    Tip skini(){
        if(br_elemenata==0) throw std::domain_error("Stek je prazan !");
        Tip tmp=top->info;
        auto pom(top->veza);
        delete top;
        top=pom;
        pom=nullptr;
        br_elemenata--;
        return tmp;
    }
    
    Tip &vrh(){
        if(br_elemenata==0) throw std::domain_error("Stek je prazan !");
        return top->info;
    }
    
    int brojElemenata(){
        return br_elemenata;
    }
    
    Stek &operator=(const Stek &s){
        if(this==&s) return *this;
    
    while(top != nullptr) {
                Cvor *p = top->veza;
                delete top;
                top = p;
            }
     br_elemenata=s.br_elemenata;
     top=nullptr;
    
    if(br_elemenata!=0){
        auto pomocni(s.top);
        top=new Cvor;
        top->info=pomocni->info;
        top->veza=nullptr;
        auto pocetak(top);
        pomocni=pomocni->veza;
        while(pomocni!=nullptr){
           Cvor *p=new Cvor;
           p->info=pomocni->info;
           p->veza=nullptr;
           pocetak->veza=p;
           pocetak=p;
           p=nullptr;
           pomocni=pomocni->veza;
        }
    }  
    
   return *this; 
    }
};


void pretraga(Stek<std::vector<int>> &s, int trazeni){
    std::vector<int> tmp,pom;
    auto novi=s;
    int index(0);
    bool ima(0);
   
    
    for(int i=s.brojElemenata()-1; i>=0;i--){
        pom=s.vrh();
        for(int j=0;j<pom.size();j++){
            if(trazeni==pom[j]){
                index=i; ima=1;
            }
        }
        if(i<novi.brojElemenata()) s.skini();
    }
    if(ima==1){
        s=novi;
        for(int i=s.brojElemenata()-1; i>=0;i--){
        pom=s.vrh();
        for(int j=0;j<pom.size();j++){
            if(trazeni==pom[j]){
                tmp=pom;
            }
        }
        if(i<novi.brojElemenata()) s.skini();
    }
        
        int dno(0), vrh(tmp.size()-1),pozicija(0);
        while(vrh>=dno){
            int srednji=(dno+vrh)/2;
            if(tmp[srednji]==trazeni){ pozicija=srednji; break;}
            else if(tmp[srednji]>trazeni) vrh=srednji-1;
            else dno=srednji+1;
        }
        std::cout<<pozicija<<" "<<index;
        s=novi;
    }
    else std::cout<<"Nema elementa";
}

void TestPretraga1(){
    Stek<std::vector<int>> s;
    std::vector<int> v1{10,20,30,40,50},v2{76,144},v3{13,257,64},v4{987,1000,7},v5{};
    s.stavi(v1); s.stavi(v2); s.stavi(v3); s.stavi(v4); s.stavi(v5);
    pretraga(s,13);
}

void TestPretraga2(){
    Stek<std::vector<int>> s;
    std::vector<int> v1{1,3,5,7,9},v2{2,4,6,8},v3{100,1000,10000};
    s.stavi(v1); s.stavi(v2); s.stavi(v3); 
    pretraga(s,100000);
}

void TestPretraga3(){
    Stek<std::vector<int>> s;
    std::vector<int> v1(100),v2(500),v3(1000);
    for(int i=1;i<=100;i++)
    v1.push_back(i);
    s.stavi(v1);
    for(int i=500;i<=1000;i++)
    v2.push_back(i);
    s.stavi(v2);
    for(int i=1000; i<=2000;i++)
    v3.push_back(i);
    s.stavi(v3);
    pretraga(s,1500);
}

int main() {
    TestPretraga1();
    std::cout<<std::endl;
    TestPretraga2();
    std::cout<<std::endl;
    TestPretraga3();
    return 0;
}
