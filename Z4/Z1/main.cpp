#include <iostream>
#include <stdexcept>
#include <iomanip>
#include <ctime>

/*promijenjena pomocna za brisanje, padao 7.at*/

template<typename TipKljuca, typename TipVrijednosti>
class Mapa{
    public:
    Mapa(){};
    virtual ~Mapa(){};
    virtual TipVrijednosti operator[](const TipKljuca &k)const=0;
    virtual TipVrijednosti &operator[](const TipKljuca &k)=0;
    virtual int brojElemenata() const=0;
    virtual void obrisi()=0;
    virtual void obrisi(const TipKljuca &k)=0;
};


template<typename TipKljuca, typename TipVrijednosti>
class BinStabloMapa: public Mapa<TipKljuca, TipVrijednosti>{
    private:
    int br_elemenata;
    struct Cvor{
        TipKljuca kljuc;
        TipVrijednosti info;
        Cvor* lijevo;
        Cvor *desno;
        Cvor *roditelj;
    };
    Cvor *korijen;
  
    Cvor* &pomocnaZaKonstruktore(Cvor* c, Cvor* &novi, Cvor* roditelj){
        if(c==nullptr) novi=nullptr;
        else{
            novi=new Cvor;
            novi->kljuc=c->kljuc;
            novi->info=c->info;
            novi->roditelj=roditelj;
            pomocnaZaKonstruktore(c->lijevo,novi->lijevo ,novi);
            pomocnaZaKonstruktore(c->desno,novi->desno ,novi);
            if(novi->roditelj==nullptr) return novi;
        }
    }
    
    void pomocnaZaBrisanjeSvega(Cvor* r){
        if(r==nullptr) return;
        else{
            if(r->lijevo!=nullptr) pomocnaZaBrisanjeSvega(r->lijevo);
            if(r->desno!=nullptr) pomocnaZaBrisanjeSvega(r->desno);
            if(r!=nullptr) delete r;
        }
    }
    
    void pomocnaZaBrisanje(Cvor *tmp){
        if(tmp==nullptr) return;
        if(tmp->lijevo==nullptr && tmp->desno==nullptr){
                if(tmp==tmp->roditelj->lijevo) tmp->roditelj->lijevo=nullptr;
                else tmp->roditelj->desno=nullptr;
                delete tmp;
                br_elemenata--;
            }
            else if(tmp->lijevo!=nullptr && tmp->desno!=nullptr){
                Cvor *pom(tmp->lijevo);
                while(pom->desno!=nullptr) pom=pom->desno;
                tmp->kljuc=pom->kljuc;
                tmp->info=pom->info;
                pomocnaZaBrisanje(pom);
            }
            else{
                Cvor* pom;
                if(tmp->lijevo==nullptr) pom=tmp->desno;
                else pom=tmp->lijevo;
                if(tmp->roditelj==nullptr){
                    pom->roditelj=tmp->roditelj;
                    delete tmp;
                    korijen=pom;
                    br_elemenata--;
                }
                else if(tmp==tmp->roditelj->lijevo) {
                    tmp->roditelj->lijevo=pom;
                    pom->roditelj=tmp->roditelj;
                    delete tmp;
                    br_elemenata--;
                }
                else {
                tmp->roditelj->desno=pom;
                pom->roditelj=tmp->roditelj;
                delete tmp;
                br_elemenata--;
                }
            }
    }
    
    Cvor* pomocnaZaTrazenje(Cvor *korijen, const TipKljuca &k){
        if(korijen==nullptr || korijen->kljuc==k) return korijen;
        else if(k<korijen->kljuc) return pomocnaZaTrazenje(korijen->lijevo, k);
        return pomocnaZaTrazenje(korijen->desno, k);
    }
    
    TipVrijednosti &pomocnaZaOperator(Cvor *korijen, const TipKljuca &k){
        if(korijen->kljuc==k) return korijen->info;
        else if(korijen==nullptr){
            korijen->roditelj=nullptr;
            korijen->desno=nullptr;
            korijen->lijevo=nullptr;
            korijen->kljuc=k;
            korijen->info=TipVrijednosti();
            br_elemenata++;
            return korijen->info;
        }
        else if(korijen->kljuc<k && korijen->desno==nullptr){
            korijen->desno=new Cvor;
            korijen->desno->kljuc=k;
            korijen->desno->desno=nullptr;
            korijen->desno->lijevo=nullptr;
            korijen->desno->roditelj=korijen;
            korijen->desno->info=TipVrijednosti();
            br_elemenata++;
            return korijen->desno->info;
        }
        else if(korijen->kljuc>k && korijen->lijevo==nullptr){
            korijen->lijevo=new Cvor;
            korijen->lijevo->kljuc=k;
            korijen->lijevo->desno=nullptr;
            korijen->lijevo->lijevo=nullptr;
            korijen->lijevo->roditelj=korijen;
            korijen->lijevo->info=TipVrijednosti();
            br_elemenata++;
            return korijen->lijevo->info;
        }
        else if(korijen->kljuc<k && korijen->desno!=nullptr) return pomocnaZaOperator(korijen->desno, k);
        return pomocnaZaOperator(korijen->lijevo,k);
    }
    
    Cvor* maxBst(Cvor *korijen){
        if(korijen->desno==nullptr) return korijen;
        return maxBst(korijen->desno);
    }
    
    TipVrijednosti pomocnaZaKljuc(Cvor *korijen, const TipKljuca &k) const{
        if(korijen==nullptr) return TipVrijednosti();
        if(korijen->kljuc==k) return korijen->info;
        if(korijen->kljuc<k) return pomocnaZaKljuc(korijen->desno, k);
        return pomocnaZaKljuc(korijen->lijevo, k);
    }
        
    public:
    ~BinStabloMapa(){
        pomocnaZaBrisanjeSvega(korijen);
    }
    
    BinStabloMapa(): br_elemenata(0), korijen(nullptr) {}
    
    BinStabloMapa(const BinStabloMapa &m): br_elemenata(m.br_elemenata) {
        br_elemenata=m.br_elemenata;
        if(br_elemenata!=0){ 
            korijen=pomocnaZaKonstruktore(m.korijen, korijen, nullptr);
        }
        else {
            korijen=nullptr;
        }
    }
    
    BinStabloMapa  &operator=(const BinStabloMapa<TipKljuca, TipVrijednosti> &m){
        if(this==&m) return *this;
        if(br_elemenata!=0) obrisi();
        br_elemenata=m.br_elemenata;
        if(br_elemenata!=0)
        korijen=pomocnaZaKonstruktore(m.korijen, korijen, nullptr);
        else korijen=nullptr;
        return *this;
    }
    
    TipVrijednosti operator[](const TipKljuca &k)const{
        return pomocnaZaKljuc(korijen, k);
    }
    
    TipVrijednosti &operator[](const TipKljuca &k){
        if(br_elemenata==0){
            korijen=new Cvor;
            korijen->kljuc=k;
            korijen->roditelj=nullptr;
            korijen->desno=nullptr;
            korijen->lijevo=nullptr;
            br_elemenata++;
            korijen->info=TipVrijednosti();
            return korijen->info;
        }
        else if(korijen->kljuc==k) return korijen->info;
        return pomocnaZaOperator(korijen, k);
    }
    
    int brojElemenata() const {
        return br_elemenata;
    }
    
    void obrisi(){
        pomocnaZaBrisanjeSvega(korijen);
        korijen=nullptr;
        br_elemenata=0;
    }
    
    void obrisi(const TipKljuca &k){
        Cvor* tmp(pomocnaZaTrazenje(korijen, k));
        if(tmp==nullptr) return;
        pomocnaZaBrisanje(tmp);
    }
};

template<typename TipKljuca, typename TipVrijednosti>
class AVLStabloMapa: public Mapa<TipKljuca, TipVrijednosti>{
    private:
    int br_elemenata;
    struct Cvor{
        int balans;
        TipKljuca kljuc;
        TipVrijednosti info;
        Cvor* lijevo;
        Cvor *desno;
        Cvor *roditelj;
    };
    Cvor *korijen;
  
    Cvor* &pomocnaZaKonstruktore(Cvor* c, Cvor* &novi, Cvor* roditelj){
        if(c==nullptr) novi=nullptr;
        else{
            novi=new Cvor;
            novi->kljuc=c->kljuc;
            novi->info=c->info;
            novi->roditelj=roditelj;
            novi->balans=c->balans;
            pomocnaZaKonstruktore(c->lijevo,novi->lijevo ,novi);
            pomocnaZaKonstruktore(c->desno,novi->desno ,novi);
            if(novi->roditelj==nullptr) return novi;
        }
    }
    
    void pomocnaZaBrisanjeSvega(Cvor* r){
        if(r==nullptr) return;
        else{
            if(r->lijevo!=nullptr) pomocnaZaBrisanjeSvega(r->lijevo);
            if(r->desno!=nullptr) pomocnaZaBrisanjeSvega(r->desno);
            if(r!=nullptr) delete r;
        }
    }
    
    void pomocnaZaBrisanje(TipKljuca &k){
        /*if(tmp==nullptr) return;
        if(tmp->lijevo==nullptr && tmp->desno==nullptr){
                if(tmp==tmp->roditelj->lijevo) tmp->roditelj->lijevo=nullptr;
                else tmp->roditelj->desno=nullptr;
                delete tmp;
                br_elemenata--;
            }
            else if(tmp->lijevo!=nullptr && tmp->desno!=nullptr){
                Cvor *pom(tmp->lijevo);
                while(pom->desno!=nullptr) pom=pom->desno;
                tmp->kljuc=pom->kljuc;
                tmp->info=pom->info;
                pomocnaZaBrisanje(pom);
            }
            else{
                Cvor* pom;
                if(tmp->lijevo==nullptr) pom=tmp->desno;
                else pom=tmp->lijevo;
                if(tmp->roditelj==nullptr){
                    pom->roditelj=tmp->roditelj;
                    delete tmp;
                    korijen=pom;
                    br_elemenata--;
                }
                else if(tmp==tmp->roditelj->lijevo) {
                    tmp->roditelj->lijevo=pom;
                    pom->roditelj=tmp->roditelj;
                    delete tmp;
                    br_elemenata--;
                }
                else {
                tmp->roditelj->desno=pom;
                pom->roditelj=tmp->roditelj;
                delete tmp;
                br_elemenata--;
                }
            }*/
            Cvor *pom(korijen), *rod(nullptr), *rodpom(nullptr);
            while(pom!=nullptr && pom->kljuc!=k){
                rod=pom;
                if(pom->kljuc<k) pom=pom->desno;
                else pom=pom->lijevo;
            }
            if(pom==nullptr) return;
            if(pom->lijevo==nullptr) rodpom=pom->desno;
            else if(pom->desno==nullptr) rodpom=pom->lijevo;
            else{
                Cvor *pom2, *tmp;
                pom2=pom;
                rodpom=pom->lijevo;
                tmp=rodpom->desno;
                while(tmp!=nullptr){
                    pom2=rodpom;
                    rodpom=tmp;
                    tmp=rodpom->desno;
                }
                if(pom2!=pom){
                    pom2->desno=rodpom->lijevo;
                    rodpom->lijevo=pom->lijevo;
                }
                rodpom->desno=pom->desno;
            }
            if(rod==nullptr) korijen=rodpom;
            else if(pom==rod->lijevo) rod->lijevo=rodpom;
            else rod->desno=rodpom;
            br_elemenata--;
            delete pom;
    }
    
    Cvor* pomocnaZaTrazenje(Cvor *korijen, const TipKljuca &k){
        if(korijen==nullptr || korijen->kljuc==k) return korijen;
        else if(k<korijen->kljuc) return pomocnaZaTrazenje(korijen->lijevo, k);
        return pomocnaZaTrazenje(korijen->desno, k);
    }
    
    TipVrijednosti &pomocnaZaOperator(Cvor *korijen, const TipKljuca &k){
        if(korijen->kljuc==k) return korijen->info;
        else if(korijen==nullptr){
            korijen->roditelj=nullptr;
            korijen->desno=nullptr;
            korijen->lijevo=nullptr;
            korijen->kljuc=k;
            korijen->info=TipVrijednosti();
            br_elemenata++;
            return korijen->info;
        }
        else if(korijen->kljuc<k && korijen->desno==nullptr){
            korijen->desno=new Cvor;
            korijen->desno->kljuc=k;
            korijen->desno->desno=nullptr;
            korijen->desno->lijevo=nullptr;
            korijen->desno->roditelj=korijen;
            korijen->desno->info=TipVrijednosti();
            br_elemenata++;
            return korijen->desno->info;
        }
        else if(korijen->kljuc>k && korijen->lijevo==nullptr){
            korijen->lijevo=new Cvor;
            korijen->lijevo->kljuc=k;
            korijen->lijevo->desno=nullptr;
            korijen->lijevo->lijevo=nullptr;
            korijen->lijevo->roditelj=korijen;
            korijen->lijevo->info=TipVrijednosti();
            br_elemenata++;
            return korijen->lijevo->info;
        }
        else if(korijen->kljuc<k && korijen->desno!=nullptr) return pomocnaZaOperator(korijen->desno, k);
        return pomocnaZaOperator(korijen->lijevo,k);
    }
    
    Cvor* maxBst(Cvor *korijen){
        if(korijen->desno==nullptr) return korijen;
        return maxBst(korijen->desno);
    }
    
    TipVrijednosti pomocnaZaKljuc(Cvor *korijen, const TipKljuca &k) const{
        if(korijen==nullptr) return TipVrijednosti();
        if(korijen->kljuc==k) return korijen->info;
        if(korijen->kljuc<k) return pomocnaZaKljuc(korijen->desno, k);
        return pomocnaZaKljuc(korijen->lijevo, k);
    }
    
    int dajBalans(Cvor *c){
        if(c==nullptr) return 0;
        return c->balans;
    }
    
    void popraviBalans(Cvor *&c){
        int balansLijevo(dajBalans(c->lijevo)), balansDesno(dajBalans(c->desno));
        if(balansLijevo>balansDesno) c->balans=++balansLijevo;
        else c->balans=++balansDesno;
    }
    
    void rotirajLijevo(Cvor *&c){
        Cvor *tmp(c->desno);
        tmp->roditelj=c->roditelj;
        c->desno=nullptr;
        tmp->desno=c;
        c->roditelj=tmp;
        popraviBalans(c);
        popraviBalans(tmp);
    }
    
    void rotirajDesno(Cvor *&c){
        Cvor *tmp(c->lijevo);
        tmp->roditelj=c->roditelj;
        c->lijevo=nullptr;
        tmp->lijevo=c;
        c->roditelj=tmp;
        popraviBalans(c);
        popraviBalans(tmp);
    }
    
    void azurirajBalans(Cvor *&c){
        popraviBalans(c);
        int pom(dajBalans(c->desno)-dajBalans(c->lijevo));
        if(pom>1){
            if(dajBalans(c->desno->desno)-dajBalans(c->desno->lijevo)<0)
                rotirajDesno(c->desno);
            rotirajLijevo(c);
        }
        else if(pom<-1){
            if(dajBalans(c->lijevo->desno)-dajBalans(c->lijevo->lijevo)>0)
                rotirajLijevo(c->lijevo);
            rotirajDesno(c);
        }
        
    }
        
    public:
    ~AVLStabloMapa(){
        pomocnaZaBrisanjeSvega(korijen);
    }
    
    AVLStabloMapa(): br_elemenata(0), korijen(nullptr) {}
    
    AVLStabloMapa(const AVLStabloMapa &m): br_elemenata(m.br_elemenata) {
        br_elemenata=m.br_elemenata;
        if(br_elemenata!=0){ 
            korijen=pomocnaZaKonstruktore(m.korijen, korijen, nullptr);
        }
        else {
            korijen=nullptr;
        }
    }
    
    AVLStabloMapa  &operator=(const AVLStabloMapa<TipKljuca, TipVrijednosti> &m){
        if(this==&m) return *this;
        if(br_elemenata!=0) obrisi();
        br_elemenata=m.br_elemenata;
        if(br_elemenata!=0)
        korijen=pomocnaZaKonstruktore(m.korijen, korijen, nullptr);
        else korijen=nullptr;
        return *this;
    }
    
    TipVrijednosti operator[](const TipKljuca &k)const{
        return pomocnaZaKljuc(korijen, k);
    }
    
    TipVrijednosti &operator[](const TipKljuca &k){
        if(br_elemenata==0){
            korijen=new Cvor;
            korijen->kljuc=k;
            korijen->roditelj=nullptr;
            korijen->desno=nullptr;
            korijen->lijevo=nullptr;
            korijen->balans=0;
            br_elemenata++;
            korijen->info=TipVrijednosti();
            return korijen->info;
        }
        else if(korijen->kljuc==k) return korijen->info;
       return pomocnaZaOperator(korijen, k);
    }
    
    int brojElemenata() const {
        return br_elemenata;
    }
    
    void obrisi(){
        pomocnaZaBrisanjeSvega(korijen);
        korijen=nullptr;
        br_elemenata=0;
    }
    
    void obrisi(const TipKljuca &k){
        Cvor* tmp(pomocnaZaTrazenje(korijen, k));
        if(tmp==nullptr) return;
        pomocnaZaBrisanje(tmp->kljuc);
        
    }
};

int main() {
    std::cout << "Zadaća 4, Zadatak 1"<<std::endl;
    
    BinStabloMapa<int, int> stablo;
    for(int i=0; i<5000; i++)
        stablo[i]=i; 
    AVLStabloMapa<int, int> avl;
    for(int i=0; i<5000; i++)
        avl[i]=i; 
    
    clock_t vrijemeDodavanjaStablo1=clock();
    stablo[10000]=6666;
    clock_t vrijemeDodavanjaStablo2=clock();
    int ukVrijemeDodavanjaStablo=(vrijemeDodavanjaStablo2-vrijemeDodavanjaStablo1)/(CLOCKS_PER_SEC / 1000);
    
    clock_t vrijemeDodavanjaAvl1=clock();
    avl[10000]=6666;
    clock_t vrijemeDodavanjaAvl2=clock();
    int ukVrijemeDodavanjaAvl=(vrijemeDodavanjaAvl2-vrijemeDodavanjaAvl1)/(CLOCKS_PER_SEC / 1000);
    
    std::cout<<"Vrijeme dodavanja novog elementa binarno stablo je: "<<ukVrijemeDodavanjaStablo<<"ms, u AVL stablo: "<<ukVrijemeDodavanjaAvl<<"ms."<<std::endl;
    
    clock_t vrijemePristupaStablo1=clock();
    std::cout<<stablo[333]<<std::endl;
    clock_t vrijemePristupaStablo2=clock();
    int ukVrijemePristupaStablo=(vrijemePristupaStablo2-vrijemePristupaStablo1)/(CLOCKS_PER_SEC / 1000);
    
    clock_t vrijemePristupaAvl1=clock();
    std::cout<<avl[333]<<std::endl;
    clock_t vrijemePristupaAvl2=clock();
    int ukVrijemePristupaAvl=(vrijemePristupaAvl2-vrijemePristupaAvl1)/(CLOCKS_PER_SEC / 1000);
    
    std::cout<<"Vrijeme pristupa elementu binarnog stabla je: "<<ukVrijemePristupaStablo<<"ms, a AVL stabla: "<<ukVrijemePristupaAvl<<"ms."<<std::endl;

    /*Vidimo da je dodavanje elementa u AVL stablo nešto brže nego dodavanje u binarno stablo,
    dok isto važi i za pretraživanje*/
    
        return 0;
}
