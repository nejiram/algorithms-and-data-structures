#include <iostream>
#include <stdexcept>

template<typename Tip>
class Stek{
    private:
    int br_elemenata;
    struct Cvor{
        Tip info;
        Cvor *veza;
    };
    Cvor *top;
    public:
    Stek():br_elemenata(0),top(nullptr){}
    ~Stek(){
        while(top!=nullptr){
            Cvor *p=top->veza;
            delete top;
            top=p;
        }
    }
    
    Stek(const Stek &s){
        br_elemenata=s.br_elemenata;
        if(br_elemenata!=0){
            Cvor *tmp(s.top);
            top=new Cvor;
            top->info=tmp->info;
            top->veza=nullptr;
            Cvor *poc(top);
            tmp=tmp->veza;
            if(tmp!=nullptr)
            do{
                Cvor *p=new Cvor;
                p->info=tmp->info;
                p->veza=nullptr;
                poc->veza=p;
                poc=p;
                p=nullptr;
                tmp=tmp->veza;
            }while(tmp!=nullptr);
            else top=nullptr;
        }
    }
    
    void brisi(){
        while(top!=nullptr){
            Cvor *p=top->veza;
            delete top;
            top=p;
        }
        br_elemenata=0;
    }
    
    void stavi(const Tip &el){
        if(top==nullptr){
            top=new Cvor;
            top->info=el;
            top->veza=nullptr;
            br_elemenata++;
        }
        else if(br_elemenata>0){
            Cvor *p=new Cvor;
            p->veza=top;
            p->info=el;
            top=p;
            p=nullptr;
            br_elemenata++;
        }
    }
    
    Tip skini(){
        if(br_elemenata==0) throw std::domain_error("Stek je prazan !");
        Tip tmp=top->info;
        auto pom(top->veza);
        delete top;
        top=pom;
        pom=nullptr;
        br_elemenata--;
        return tmp;
    }
    
    Tip &vrh(){
        if(br_elemenata==0) throw std::domain_error("Stek je prazan !");
        return top->info;
    }
    
    int brojElemenata(){
        return br_elemenata;
    }
    
    Stek &operator=(const Stek &s){
        if(this==&s) return *this;
        while(top!=nullptr){
            Cvor *p=top->veza;
            delete top;
            top=p;
        }
        br_elemenata=s.br_elemenata;
        top=nullptr;
        if(br_elemenata>0){
            auto tmp(s.top);
            top=new Cvor;
            top->info=tmp->info;
            top->veza=nullptr;
            auto poc(top);
            tmp=tmp->veza;
            do{
                Cvor *p=new Cvor;
                p->info=tmp->info;
                p->veza=nullptr;
                poc->veza=p;
                poc=p;
                p=nullptr;
                tmp=tmp->veza;
            }while(tmp!=nullptr);
        }
        return *this;
    }
};

/*testne funkcije*/
void Test_Stavi(){
    Stek<double> s;
    for(auto i=0.5;i<=100;i+=0.5)
    s.stavi(i);
    std::cout<<s.brojElemenata()<<std::endl;
}

void Test_Skini(){
    Stek<int> s;
    for(int i=0; i<1000;i++)
    s.stavi(i);
    for(int i=0;i<900;i++)
    s.skini();
    std::cout<<s.brojElemenata()<<std::endl;
}

void Test_Kopi_konstr_Vrh(){
    Stek<char> s1;
    for(auto i='A';i<='Z';i++)
    s1.stavi(i);
    auto s2(s1);
    std::cout<<s2.vrh()<<std::endl;
}

void Test_Brisi(){
    Stek<double> s;
    for(auto i=0.2;i<10;i+=0.2)
    s.stavi(i);
    s.brisi();
    std::cout<<s.brojElemenata()<<std::endl;
}

void Test_Dodjela(){
    Stek<int> s1;
    for(auto i=0;i<1000000;i++)
    s1.stavi(i);
    auto s2=s1;
    std::cout<<s2.brojElemenata()<<std::endl;
}


int main() {
    Test_Stavi();
    Test_Skini();
    Test_Kopi_konstr_Vrh();
    Test_Brisi();
    Test_Dodjela();
    return 0;
}
